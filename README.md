# README #

### What is this repository for? ###

This repository contains the code for a game I am working on called Vertigo. Vertigo will be a puzzle game where the player must push and pull cubes in order to climb a tower.
This game was heavily inspired by Catherine, and as of right now the base engine is working which includes:

- A* Pathfinding in a 3d cube based world
- World that keeps track of cubes and what states they are in/what state they should be in during the next frame
- Level Selection
- Semi-dynamic mesh building
- Custom made shop screen with scrolling selections