﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coordinate {
	public int x;
	public int y;
	public int z;
	public Coordinate(){
	}
	public Coordinate(int x1, int y1, int z1){
		x = x1;
		y = y1;
		z = z1;
	}
}
