﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUIController : MonoBehaviour {

	public Text ownedText;
	public Text priceText;
	public GameObject mainCam;
	public GameObject shopCam;
	public GameObject shopObject;


	bool showAll;
	GameObject mainMenuObject;
	GameObject levelSelectUI;
	GameObject fillerCubes;
	GameObject acceptButton;
	GameObject purchaseButton;
	GameObject ownedButton;
	GameObject shopButton;
	GameObject backButton;
	Sprite pressedSprite;
	Sprite unpressedSprite;
	SpriteState st;
	Image ownedImage;
	Button ownedButtonComp;
	Shop[] shops = new Shop[3];
	PlayerModelBuilder pModelBuilder;
	TouchCameraController tcCont;
	PersistentMenuUIController perMenuCont;
	DataManager dataMan;

	// Use this for initialization
	void Start () {
		showAll = false;
		pModelBuilder = GetComponent<PlayerModelBuilder> ();
		shops [0] = GameObject.Find ("BodyShop").GetComponent<Shop>();
		shops [1] = GameObject.Find ("HeadShop").GetComponent<Shop>();
		shops [2] = GameObject.Find ("HatShop").GetComponent<Shop>();
		fillerCubes = GameObject.Find ("FillerCubes");

		purchaseButton = transform.Find ("PurchaseButton").gameObject;
		acceptButton = transform.Find ("AcceptButton").gameObject;
		ownedButton = transform.Find ("OwnedButton").gameObject;
		shopButton = transform.Find ("ShopButton").gameObject;
		backButton = transform.Find ("BackButton").gameObject;

		tcCont = FindObjectOfType<TouchCameraController> ();
		perMenuCont = FindObjectOfType<PersistentMenuUIController> ();
		dataMan = FindObjectOfType<DataManager> ();
		ownedButtonComp = ownedButton.GetComponent<Button> ();
		ownedImage = ownedButton.GetComponent<Image> ();
		pressedSprite = ownedButtonComp.spriteState.pressedSprite;
		unpressedSprite = ownedImage.sprite;

		SetShopButtons (false);
		shopCam.SetActive (false);
		mainCam.SetActive (true);
	}

	/// <summary>
	/// Toggles the menu between show owned and show all
	/// </summary>
	public void ShowAllToggle(){
		showAll = !showAll;
		if (showAll) {
			st.pressedSprite = pressedSprite;
			ownedImage.sprite = unpressedSprite;
		} else {
			st.pressedSprite = unpressedSprite;
			ownedImage.sprite = pressedSprite;
		}
		ownedButtonComp.spriteState = st;
		for (int i = 0; i < shops.Length; i++) {
			shops [i].ShowAll (showAll);
		}
	}

	/// <summary>
	/// Closes the shop, opens main menu.
	/// </summary>
	public void CloseShop(){
		perMenuCont.SetSettingsActive (true);
		shopCam.SetActive (false);
		fillerCubes.SetActive (true);
		tcCont.SetActive (true);
		mainCam.SetActive (true);
		//Reset show all so when the shop reopens it is set to true
		showAll = false;
		ShowAllToggle ();
		shopObject.SetActive (false);
		SetShopButtons (false);
	}

	/// <summary>
	/// Opens the shop, closes the main menu.
	/// </summary>
	public void OpenShop(){
		//Disabled settings button
		perMenuCont.SetSettingsActive (false);
		//Don't need to render the cubes
		fillerCubes.SetActive (false);
		tcCont.SetActive (false);
		shopObject.SetActive (true);
		//Render the shop buttons
		SetShopButtons (true);
		shopCam.SetActive (true);
		mainCam.SetActive (false);
		showAll = true;
	}

	void SetShopButtons(bool enabled){
		purchaseButton.SetActive(enabled);
		acceptButton.SetActive(enabled);
		ownedButton.SetActive (enabled);
		backButton.SetActive (enabled);
		shopButton.SetActive (!enabled);
	}

	/// <summary>
	/// Accept button on shop has been pressed, build a new model
	/// </summary>
	public void AcceptShop(){
		dataMan.SetHat (shops [2].GetCenter ().GetModelName ());
		dataMan.SetHead (shops [1].GetCenter ().GetModelName ());
		dataMan.SetBody (shops [0].GetCenter ().GetModelName ());
		pModelBuilder.BuildModel ();
		CloseShop ();
	}

	public void ToggleAccept(bool toggle){
		if (shopCam.activeSelf == true) {
			acceptButton.SetActive (toggle);
			purchaseButton.SetActive (!toggle);
		}
	}
}
