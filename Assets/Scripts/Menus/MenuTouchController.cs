using UnityEngine;

namespace Lean.Touch
{
	// This script allows you to transform the current GameObject
	public class MenuTouchController: MonoBehaviour
	{
		public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;
		[Tooltip("Ignore fingers with StartedOverGui?")]
		public bool IgnoreGuiFingers = true;

		[Tooltip("Allows you to force rotation with a specific amount of fingers (0 = any)")]
		public int RequiredFingerCount;
	
		public SwipeMenuItem curItem;
		public GameObject ShopObject;

		TouchCameraController tcCont;

		void Start(){
			tcCont = FindObjectOfType<TouchCameraController>();
		}

		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerDown += OnFingerDown;
			LeanTouch.OnFingerUp += OnFingerUp;
		}

		protected virtual void OnDisable()
		{
			// Unhook from the events
			LeanTouch.OnFingerDown -= OnFingerDown;
			LeanTouch.OnFingerUp -= OnFingerUp;
		}

		private void OnFingerDown(LeanFinger finger)
		{
			if (this.enabled == true) {
				if (!tcCont.GetActive ()) {
					var ray = finger.GetStartRay ();
					var hit = default(RaycastHit);
					// Was this finger pressed down on a collider?
					if (Physics.Raycast (ray, out hit, float.PositiveInfinity, LayerMask) == true) {
						// Was that collider a cube?
						if (hit.collider.gameObject.tag == "MenuItem") {
							curItem = hit.collider.GetComponent<SwipeMenuItem> ();
							curItem.parMenu.ResetMoving ();
						}
					}
				} else {
					tcCont.ResetMoving();
				}
			}
		}

		private void OnFingerUp(LeanFinger finger)
		{
			if (this.enabled == true) {
				if (curItem != null) {
					// Convert this GameObject's world position into screen coordinates and store it in a temp variable
					var screenPosition = Camera.main.WorldToScreenPoint (transform.position);

					// Modify screen position by the finger's delta screen position over the past 0.1 seconds
					screenPosition += (Vector3)finger.GetSnapshotScreenDelta (0.1f);

					// Convert the screen position into world coordinates and subtract it by the old position to find the world delta over the past 0.1 seconds
					var worldDelta = Camera.main.ScreenToWorldPoint (screenPosition) - transform.position;

					Vector3 newVelocity = new Vector3 (worldDelta.x / 0.1f, 0, 0);
					curItem.parMenu.SetMoving (newVelocity);
					curItem = null;
				} else if (tcCont.GetActive ()) {
					// Convert this GameObject's world position into screen coordinates and store it in a temp variable
					var screenPosition = Camera.main.WorldToScreenPoint (transform.position);

					// Modify screen position by the finger's delta screen position over the past 0.1 seconds
					screenPosition += (Vector3)finger.GetSnapshotScreenDelta (0.1f);

					// Convert the screen position into world coordinates and subtract it by the old position to find the world delta over the past 0.1 seconds
					var worldDelta = Camera.main.ScreenToWorldPoint (screenPosition) - transform.position;

					Vector3 newVelocity = new Vector3 (0, -(worldDelta.y / 0.1f), 0);
					tcCont.SetMoving (newVelocity);
				}
			}
		}

		protected virtual void Update()
		{
			if (this.enabled == true) {
				if (curItem != null || tcCont.GetActive ()) {
					// Get the fingers we want to use
					var fingers = LeanTouch.GetFingers (IgnoreGuiFingers, RequiredFingerCount);

					// Calculate the screenDelta value based on these fingers
					var screenDelta = LeanGesture.GetScreenDelta (fingers);

					// Perform the translation
					Translate (screenDelta);
				}
			}
		}

		private void Translate(Vector2 screenDelta)
		{

			// Screen position of the transform
			var screenPosition = Camera.main.WorldToScreenPoint (transform.position);
			
			// Add the deltaPosition
			screenPosition += (Vector3)screenDelta;

			if (curItem != null) {
				// Convert back to world space
				curItem.parMenu.SetPosition (new Vector3 (Camera.main.ScreenToWorldPoint (screenPosition).x, curItem.transform.position.y, curItem.transform.position.z));
			} else if(tcCont.GetActive()) {
				tcCont.AddToPosition (-Camera.main.ScreenToWorldPoint (screenPosition).y);
			}
		}
	}
}