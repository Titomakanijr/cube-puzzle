﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Attach to any sub-menu item. See Multiple Menu example scene for usage.
/// </summary>
public class SubMenuItem : MonoBehaviour
{
	/// <summary>
	/// The menu item who owns this sub-menu.
	/// </summary>
	public SwipeMenuItem OwnerMenu;

	/// <summary>
	/// The behaviour to be invoked when this sub-menu is selected.
	/// </summary>
	public Button.ButtonClickedEvent OnClick;
	public Shop parMenu;

	void Start(){
		parMenu = GetComponentInParent<Shop> ();
	}
	void Update ()
	{
			
	}
	
	private void CheckTouch (Vector3 screenPoint)
	{
		Ray touchRay = Camera.main.ScreenPointToRay (screenPoint);
		RaycastHit hit;
		
		Physics.Raycast (touchRay, out hit);
		
		if (hit.collider != null && hit.collider.gameObject.Equals (gameObject)) {
			
			OnClick.Invoke ();
		}
	}
}
