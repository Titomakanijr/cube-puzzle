﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


/// <summary>
/// Attach to any menu item. 

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShopMeshController))]
public class SwipeMenuItem : MonoBehaviour
{
	public bool owned;
	public Shop parMenu;
	public Rigidbody rb;
	public ShopMeshController smCont;

	float modelHeight;
	float bottomY;
	bool init;
	string modelName;

	void Awake(){
		//READ FROM PLAYERPREFS TO GET IF ITS OWNED OR NOT
		parMenu = GetComponentInParent<Shop> ();
		rb = GetComponent<Rigidbody> ();
		rb.useGravity = false;
		rb.drag = 2;
		smCont = GetComponent<ShopMeshController> ();
		init = true;
		modelName = smCont.modelPrefab.name;
	}

	void Update(){
		if (init) {
			if (transform.childCount > 0){
				bottomY = transform.GetChild (0).position.y;
			}
			init = false;
		}
	}

	public bool GetOwned(){
		return owned;
	}

	public string GetModelName(){
		return modelName;
	}

	public void SetHeight(float height){
		modelHeight = height;
	}

	public float GetHeight(){
		return modelHeight;
	}

	public void SetBottom(float bottom){
		bottomY = bottom;
	}

	public float GetBottom(){
		return bottomY;
	}
		
}