﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopMeshController : MonoBehaviour {


	public struct CharacterModelTrans {
		public Transform Hat, Head, ArmL, ArmR, HandL, HandR;
		public Transform Body, LegL, LegR, FootL, FootR;
	}
	public CharacterModelTrans chMTrans;

	public bool bodyShop;
	public bool headShop;
	public bool hatShop;

	public GameObject modelPrefab;

	SwipeMenuItem item;


	// Use this for initialization
	void Start () {
		item = GetComponent<SwipeMenuItem> ();
		if (hatShop && modelPrefab.name == "None") {
			return;
		}
		GameObject model = Instantiate (modelPrefab, new Vector3(0, gameObject.transform.position.y, 0), Quaternion.identity) as GameObject;
		if (bodyShop) {
			Destroy(model.transform.Find("Body/Head").gameObject);
			chMTrans.LegL = model.transform.Find("Body/LegL");
			chMTrans.LegR = model.transform.Find("Body/LegR");
			chMTrans.FootL = model.transform.Find("Body/LegL/FootL");
			chMTrans.FootR = model.transform.Find("Body/LegR/FootR");

			chMTrans.Body = model.transform.Find("Body");
			chMTrans.ArmL = model.transform.Find("Body/ArmL");
			chMTrans.ArmR = model.transform.Find("Body/ArmR");
			chMTrans.HandL = model.transform.Find("Body/ArmL/HandL");
			chMTrans.HandR = model.transform.Find("Body/ArmR/HandR");

			Transform temp = model.transform.Find ("Body/Body_Model");
			item.SetHeight (Utils.MeshUtil.GetMeshSize(temp).y);
			item.SetBottom(Utils.MeshUtil.GetExtremeAverage (temp, Utils.MeshUtil.Direction.Bottom).y);

			chMTrans.Body.SetParent(gameObject.transform);

		} else if (headShop) {
			chMTrans.Hat = model.transform.Find("Body/Head/Hat");
			if (chMTrans.Hat != null) {
				Destroy(model.transform.Find("Body/Head/Hat").gameObject);
			}

			chMTrans.Head = model.transform.Find("Body/Head");

			Transform temp = model.transform.Find ("Body/Head/Head_Model");
			item.SetHeight (Utils.MeshUtil.GetMeshSize(temp).y);
			item.SetBottom(Utils.MeshUtil.GetExtremeAverage (temp, Utils.MeshUtil.Direction.Bottom).y);

			chMTrans.Head.SetParent(gameObject.transform);


		} else if (hatShop) {
			chMTrans.Hat = model.transform;
			chMTrans.Hat.name = chMTrans.Hat.name.Replace ("(Clone)", "").Trim ();
			Vector3 tempScale = model.transform.localScale;
			Transform temp = model.transform.Find ("Hat_Model");
			item.SetHeight (Utils.MeshUtil.GetMeshSize(temp).y * .5f);
			item.SetBottom(Utils.MeshUtil.GetExtremeAverage (temp, Utils.MeshUtil.Direction.Bottom).y);
			chMTrans.Hat.SetParent (gameObject.transform);
			chMTrans.Hat.localPosition = Vector3.zero;
			temp.localPosition = Vector3.zero;
			chMTrans.Hat.localScale = new Vector3 (.5f, .5f, .5f);
		}
		if (!hatShop) {
			Destroy (model);
		}
	}
}
