﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {


	public float centerItemOffset = 2f;
	public float startingY;

	int center;
	int shopType;
	float itemDistance = 1f;
	float currentMenuPos= 0.0f;
	float centerTimer;
	float centerTime = .5f;
	bool moving;
	bool centering;
	bool resetY;
	List<float> headBottomY;
	List<SwipeMenuItem> ownedMenuItems = new List<SwipeMenuItem> ();
	List<SwipeMenuItem> fullMenuItems = new List<SwipeMenuItem> ();
	List<SwipeMenuItem> menuItems;
	ShopController sc;

	// Use this for initialization
	void Start () {
		if (name == "BodyShop") {
			shopType = 0;
		} else if (name == "HeadShop") {
			shopType = 1;
		} else {
			shopType = 2;
		}
		sc = transform.parent.GetComponent<ShopController> ();
		for (int i = 0; i < transform.childCount; i++) {
			SwipeMenuItem tempItem = transform.GetChild (i).GetComponent<SwipeMenuItem> ();
			fullMenuItems.Add (tempItem);
			if(tempItem.GetOwned()){
				ownedMenuItems.Add (tempItem);
			}
		}
		ShopInit (true);
		CenterEquip ();
	}

	// Update is called once per frame
	void Update () {
		UpdateItemPositions ();
		for (int i = 0; i < menuItems.Count; i++) {
			UpdateZPosition (menuItems [i]);
		}
	}

	/// <summary>
	/// Updates the menu items position and rotation in world space.
	/// </summary>
	private void UpdateItemPositions ()
	{
		if (!centering) {
			//Currently Moving
			if (moving) {
				if (Mathf.Abs (menuItems [center].rb.velocity.x) < .5f){
					moving = false;
					if ((Mathf.Abs (menuItems [center].rb.velocity.x) >= 0)) {
						int closest = GetClosestMenuItemIndex ();
						centerTimer = 0f;
						center = closest;
						currentMenuPos = itemDistance * closest;
						CenterItem (center);
						centering = true;
					}
				}
			}
		//Currently centering
		}else {
			if (menuItems [center].transform.position.x == 0) {
				moving = false;
				centering = false;
			} else {
				CenterItem (center);
			}
		}
	}

	/// <summary>
	/// Centers the given SwipeMenuItem.
	/// </summary>
	/// <param name="i">SwipeMenuItem to be centered</param>
	public void CenterItem(SwipeMenuItem i){
		for (int j = 0; j < menuItems.Count; j++) {
			if (menuItems [j] == i) {
				centerTimer = 0f;
				currentMenuPos = itemDistance * j;
				center = j;
				centering = true;
				CenterItem (j);
				break;
			}
		}
	}

	/// <summary>
	/// Centers the item at index i.
	/// </summary>
	/// <param name="i">The item index.</param>
	void CenterItem(int i){
		centerTimer += Time.deltaTime / centerTime;
		float newVelocity = Mathf.Lerp (menuItems [i].transform.position.x, 0, centerTimer);
		for (int j = 0; j< menuItems.Count; j++) {
			menuItems [j].transform.position = new Vector3(newVelocity + itemDistance * (j) - currentMenuPos, menuItems [j].transform.position.y, menuItems [j].transform.position.z);
			menuItems [j].rb.velocity = new Vector3 (0, 0, 0);
		}
	}


	/// <summary>
	/// Updates x, y, and z position of a given item
	/// </summary>
	/// <param name="item">Item that will be checked for center</param>
	void UpdateZPosition(SwipeMenuItem item){
		float itemX = item.transform.position.x;
		//Check if this item is within the range that we want to update
		if (itemX < itemDistance && itemX > -itemDistance) {
			float distanceBetweenWaypoints;
			if (itemX > 0) {
				distanceBetweenWaypoints = (itemDistance - itemX) / itemDistance;
			} else {
				distanceBetweenWaypoints = (itemDistance - Mathf.Abs (itemX)) / itemDistance;
			}
			distanceBetweenWaypoints = Mathf.Clamp01 (distanceBetweenWaypoints);
			float posZ = Mathf.Lerp (0, -centerItemOffset, distanceBetweenWaypoints);
			float posY;
			//The body goes at position 0
			if (shopType == 0) {
				posY = 0;
			} else if (shopType == 1) {
				//Looking at a head, this should rest on the top of whatever body is below it
				posY = Mathf.Lerp (startingY, sc.GetBottomFromUnder (shopType) - item.GetBottom (), distanceBetweenWaypoints);
				//We are looking at a hat, this should be one head height up
			} else {
				//Make sure if the head moved for a body the hat is sitting on top of it
				float headY = sc.GetBottomFromUnder (shopType - 1) - sc.GetBottomFromUnder (shopType);
				float headHeight = sc.GetUnderItemHeight (shopType);
				posY = Mathf.Lerp (startingY, headHeight + headY, distanceBetweenWaypoints);
			}
			item.transform.position = new Vector3 (item.transform.position.x, posY, posZ);
		} else {
			//Bodies should now move in the y direction
			if (shopType == 0) {
				item.transform.position = new Vector3 (item.transform.position.x, 0, 0);
			} else {
				item.transform.position = new Vector3 (item.transform.position.x, startingY, 0);
			}
		}
	}

	/// <summary>
	/// Calculates the menu item Z position. Based on Menu#distanceBetweenSelectedMenuAndOthers.
	/// </summary>
	/// <returns>The menu item Z position.</returns>
	private float CalculateMenuItemZPosition (float offsetx)
	{
		if (offsetx == 0) {
			return -centerItemOffset;
		} else {
			return 0;
		}

	}

	/// <summary>
	/// Returns the index of the menu item closest to centre.
	/// </summary>
	/// <returns>The closest menu item index.</returns>
	private int GetClosestMenuItemIndex ()
	{
		int index = -1;

		float xOffset = float.MaxValue;

		for (int i = 0; i < menuItems.Count; i++) {
			var x = Mathf.Abs(menuItems [i].gameObject.transform.position.x);

			if (x == 0)
				return i;

			if (x < xOffset) {
				index = i;
				xOffset = x;
			}
		}

		return index;
	}

	/// <summary>
	/// Returns true if the specified menu item is centered.
	/// </summary>
	public bool IsCenter (SwipeMenuItem item)
	{
		return item.transform.position.x == 0;
	}

	/// <summary>
	/// The shop is no longer being interacted with, start movement again
	/// </summary>
	public void SetMoving(Vector3 newVelocity){
		//check if Finger was moving when released
		if (newVelocity != Vector3.zero) {
			moving = true;
			for (int i = 0; i < menuItems.Count; i++) {
				menuItems [i].rb.velocity = newVelocity;
			}
		//Finger was not moving, center the shop
		} else {
			centering = true;
			centerTimer = 0f;
			CenterItem (center);
		}
	}

	/// <summary>
	/// Creates a new shop that either shows all items or only owned items
	/// </summary>
	void ShopInit(bool showAll){
		if (showAll) {
			menuItems = fullMenuItems;
		} else {
			menuItems = ownedMenuItems;
		}
		for (int i = 0; i < menuItems.Count; i++) {
			menuItems [i].gameObject.SetActive (true);
			float offsetx = itemDistance * (i) - currentMenuPos;
			float posz = CalculateMenuItemZPosition (offsetx);
			Vector3 pos = new Vector3 (offsetx, startingY, posz);
			menuItems [i].transform.position = pos;
			Vector3 euler = new Vector3 (0, 0, 0);
			menuItems [i].transform.eulerAngles = euler;
		}

	}
		
	/// <summary>
	/// Toggles between showing all items and just owned items
	/// </summary>
	public void ShowAll(bool showAll){
		ClearShop ();
		SwipeMenuItem tempCenter = menuItems [center];
		ShopInit (showAll);
		if (!showAll) {
			if (!menuItems.Contains (tempCenter)) {
				CenterEquip ();
			} else {
				CenterItem (tempCenter);
			}
		} else {
			CenterItem (tempCenter);
		}
	}
		

	/// <summary>
	/// center a shop around the currently equipped item
	/// </summary>
	void CenterEquip(){
		string equip = PlayerPrefs.GetString ("Player" + gameObject.name.Replace("Shop", ""));
		for (int i = 0; i < menuItems.Count; i++) {
			if (menuItems [i].GetModelName() == equip) {
				CenterItem (menuItems [i]);
				center = i;
			}
		}
	}

	/// <summary>
	/// Sets all Menu Items game objects to disabled so we can create a new shop
	/// </summary>
	void ClearShop(){
		for (int i = 0; i < menuItems.Count; i++) {
			menuItems [i].gameObject.SetActive (false);
		}
	}

	/// <summary>
	/// shop is being interacted with, stop all movement
	/// </summary>
	public void ResetMoving(){
		moving = false;
		centering = false;
	}

	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="newPos">New position.</param>
	public void SetPosition(Vector3 newPos){
		for (int i = 0; i < menuItems.Count; i++) {
			menuItems [i].transform.position = new Vector3 (newPos.x + menuItems[i].transform.position.x, menuItems[i].transform.position.y, menuItems[i].transform.position.z);
		}
	}

	/// <summary>
	/// Gets the center Item in a shop.
	/// </summary>
	/// <returns>The cente Item in a shopr</returns>
	public SwipeMenuItem GetCenter(){
		return menuItems [center];
	}

	public List<SwipeMenuItem> GetOwnedItems(){
		return ownedMenuItems;
	}

	/// <summary>
	/// Returns bottom value of the item attached to the supplied name
	/// </summary>
	/// <returns>bottom value of the item attached to the supplied name.</returns>
	/// <param name="name">Model Name.</param>
	public float GetItemInitBottom(string name){
		for (int i = 0; i < fullMenuItems.Count; i++) {
			if (fullMenuItems [i].GetModelName() == name) {
				return fullMenuItems [i].GetBottom ();
			} 
		}
		return -1;
	}

	/// <summary>
	/// Returns height value of the item attached to the supplied name
	/// </summary>
	/// <returns>height value of the item attached to the supplied name.</returns>
	/// <param name="name">Model Name</param>
	public float GetItemInitHeight(string name){
		for (int i = 0; i < fullMenuItems.Count; i++) {
			if (fullMenuItems [i].GetModelName() == name) {
				return fullMenuItems [i].GetHeight ();
			} 
		}
		return -1;
	}
}
