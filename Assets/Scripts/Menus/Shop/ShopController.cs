﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour {

	Shop[] shops = new Shop[3];
	MainMenuUIController menuUI;

	// Use this for initialization
	void Start () {
		menuUI = FindObjectOfType<MainMenuUIController> ();
		shops[0] = transform.Find ("BodyShop").GetComponent<Shop>();
		shops[1] = transform.Find ("HeadShop").GetComponent<Shop>();
		shops[2] = transform.Find("HatShop").GetComponent<Shop>();
	}

	void Update(){
		for (int i = 0; i < shops.Length; i++) {
			if (!shops [i].GetCenter ().GetOwned ()) {
				menuUI.ToggleAccept (false);
				break;
			}
			menuUI.ToggleAccept (true);
		}
	}
	
	public float GetUnderItemHeight(int shopType){
		if (shopType == 0) {
			return 0;
		} else {
			return shops [shopType - 1].GetCenter ().GetHeight();
		}
	}

	/// <summary>
	/// Gets the index of the bottom.
	/// </summary>
	/// <returns>The bottom index.</returns>
	/// <param name="shopType">Shop type.</param>
	/// <param name="modelName">Model name.</param>
	public float GetBottomFromUnder(int shopType){
		string modName = shops [shopType - 1].GetCenter().GetModelName();
		if (shopType == 2) {
			return shops [shopType - 1].GetItemInitBottom (modName);
		} else {
			return shops [shopType].GetItemInitBottom (modName);
		}
	}
}
