﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCameraController : MonoBehaviour {
	public float maxHeight = 100;


	bool active;
	bool centering;
	bool limit;
	float centerTimer;
	float centerTime = .5f;
	float lerpValue;
	Transform myTransform;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		myTransform = transform;
		active = true;
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		//Camera is below the lowest point we want, move it back up
		if (myTransform.position.y < 20 && !limit) {
			SetMoving (Vector3.zero);
			lerpValue = 25;
			limit = true;
			centerTimer = 0;
		//Camera is above the highest point, move it back down
		} else if (myTransform.position.y > maxHeight && !limit) {
			SetMoving (Vector3.zero);
			lerpValue = maxHeight - 5;
			limit = true;
			centerTimer = 0;
		}
		else {
			limit = false;
		}
		//The camera is either being centered or taken back to a limit
		if (centering || limit) {
			centerTimer += Time.deltaTime / centerTime;
			float newVelocity = Mathf.Lerp (transform.position.y, lerpValue, centerTimer);
			transform.position = new Vector3 (transform.position.x, newVelocity, transform.position.z);
		}
	}

	/// <summary>
	/// Takes a value the camera should lerp to
	/// </summary>
	/// <param name="lerpY">Lerp Value</param>
	public void LerpTo(float lerpY){
		centerTimer = 0f;
		centering = true;
		lerpValue = lerpY;
	}

	/// <summary>
	/// returns the active value for touchcameracontroller
	/// </summary>
	public bool GetActive(){
		return active;
	}

	/// <summary>
	/// Sets the active value of this script
	/// </summary>
	public void SetActive(bool value){
		active = value;
	}

	/// <summary>
	/// Stops all moving of the menu camera
	/// </summary>
	public void ResetMoving(){
		centering = false;
		SetMoving (Vector3.zero);
	}

	/// <summary>
	/// Adds the y value to current camera y position
	/// </summary>
	/// <param name="y">The y coordinate.</param>
	public void AddToPosition(float y){
		myTransform.position = new Vector3 (myTransform.position.x, y + myTransform.position.y, myTransform.position.z);
	}

	public void SetPosition(float y){
		myTransform.position = new Vector3 (myTransform.position.x, y, myTransform.position.z);
	}

	/// <summary>
	/// Gives the cameras rigid body a given velocity
	/// </summary>
	/// <param name="newVelocity">New velocity</param>
	public void SetMoving(Vector3 newVelocity){
		rb.velocity = newVelocity;
	}


}
