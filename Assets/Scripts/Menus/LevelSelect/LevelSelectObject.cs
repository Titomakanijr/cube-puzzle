﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectObject : MonoBehaviour {
	public int levelNumber;

	bool active;
	bool locked;
	WorldController worldCont;

	// Use this for initialization
	void Start () {
		//check if levelCont.currentunlocked > levelNumber. Set Locked
		worldCont = FindObjectOfType<WorldController> ();
		worldCont.AddCube (new Point3D (transform.position.x, transform.position.y - 1, transform.position.z));
	}
	

	public void SetActive(bool value){
		active = value;
	}

	public bool GetActive(){
		return active;
	}

	public int GetLevelNumber(){
		return levelNumber;
	}

}
