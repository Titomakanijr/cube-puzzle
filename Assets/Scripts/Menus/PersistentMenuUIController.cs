﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersistentMenuUIController : MonoBehaviour {
	GameObject settingsObject;
	GameObject nextLevelObject;
	GameObject mainMenuObject;
	GameObject restartObject;
	GameObject pauseObject;
	GameObject endLevelMenu;
	GameObject pauseMenu;

	Button settingsButton;
	Button nextLevelButton;
	Button mainMenuButton;
	Button restartButton;
	Button pauseButton;
	Button resumeButton;
	Button pauseBackToMenuButton;
	LevelController levelCont;

	// Use this for initialization
	void Start () {
		levelCont = FindObjectOfType<LevelController> ();
		settingsObject = transform.Find ("SettingsButton").gameObject;
		pauseObject = transform.Find ("PauseButton").gameObject;
		restartObject = transform.Find ("RestartButton").gameObject;
		endLevelMenu = transform.Find ("EndLevelPanel").gameObject;
		pauseMenu = transform.Find("PausePanel").gameObject;

		settingsButton = settingsObject.GetComponent<Button> ();
		nextLevelButton = endLevelMenu.transform.Find("NextLevelButton").GetComponent<Button>();
		mainMenuButton = endLevelMenu.transform.Find("BackToMenuButton").GetComponent<Button>();
		resumeButton = pauseMenu.transform.Find ("ResumeButton").GetComponent<Button> ();
		pauseBackToMenuButton = pauseMenu.transform.Find ("BackToMenuButton").GetComponent<Button> (); 
		restartButton = restartObject.GetComponent<Button>();
		pauseButton = pauseObject.GetComponent<Button> ();

		//settingsButton.onClick.AddListener( 
		nextLevelButton.onClick.AddListener(levelCont.LoadNext);
		mainMenuButton.onClick.AddListener(levelCont.LoadMenu);
		restartButton.onClick.AddListener(levelCont.ReloadLevel);
		pauseButton.onClick.AddListener (levelCont.PauseLevel);
		resumeButton.onClick.AddListener (levelCont.UnpauseLevel);
		pauseBackToMenuButton.onClick.AddListener (levelCont.LoadMenu);
	}
	
	public void SetLevelUIActive(bool enabled){
		restartObject.SetActive (enabled);
		pauseObject.SetActive (enabled);
	}

	public void SetEndLevelMenuActive(bool enabled){
		endLevelMenu.SetActive (enabled);
		if (enabled) {
			pauseObject.SetActive (!enabled);
		}
	}

	public void SetSettingsActive(bool enabled){
		settingsObject.SetActive (enabled);
	}

	public void SetPauseMenu(bool enabled){
		pauseMenu.SetActive (enabled);
		pauseObject.SetActive (!enabled);
	}

	public void SetPauseObject(bool enabled){
		pauseObject.SetActive (enabled);
	}
}
