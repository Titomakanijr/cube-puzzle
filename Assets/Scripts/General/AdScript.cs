﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdScript : MonoBehaviour {
	[SerializeField]
	private string androidGameId = "1216976", iosGameId  = "1216977";

	[SerializeField]
	private bool testMode;
	// Use this for initialization
	void Start () {
		string gameId = null;
		gameId = androidGameId;

		#if UNITY_ANDROID
		gameId = androidGameId;
		#elif UNITY_IOS
		gameId = iosGameId;
		#endif

		if (Advertisement.isSupported && !Advertisement.isInitialized) {
			Advertisement.Initialize (gameId, testMode);
		}
	}
}
