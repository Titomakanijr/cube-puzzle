﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeIntoDestroy : MonoBehaviour {
	float timeToDestroy;

	// Use this for initialization
	void Start () {
		timeToDestroy = Random.Range (1.5f, 4f);
	}
	
	// Update is called once per frame
	void Update () {
		if (timeToDestroy > 0) {
			timeToDestroy -= Time.deltaTime;
		} else {
			Destroy (gameObject);
		}
	}
}
