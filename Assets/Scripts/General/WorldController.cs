﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour {
	public int height, width, depth;
	BombController bc;

	public World world;
	Player player;

	// Use this for initialization
	void Awake() {
		world = new World (width, height, depth);
	}

	void Start(){
		bc = GetComponent<BombController> ();
		player = FindObjectOfType<Player> ();
		player.SetWorldController (this);
	}

	/*
	 * 	<Summary>
	 * 	Takes a Point3D cube position and calls for the world to mark that 
	 * 	Position as true, or not empty
	 * 	</Summary>
	 */
	public void AddCube(Point3D cube){
		world.MarkPosition (cube, true);
	}
		
	/*
	 * 	<Summary>
	 * 	Takes a Point3D cube position and calls for the world to mark that 
	 * 	Position as false, or empty
	 * 	</Summary>
	 */
	public void RemoveCube(Point3D cubePos, bool exploded = false){
		world.MarkPosition (cubePos, false);
		world.CheckUpperCubes (cubePos, exploded);
		if (new Point3D (player.transform.position.x, player.transform.position.y - 1, player.transform.position.z) == cubePos) {
			player.FallFromBrokenCube ();
		}
	}

	/*
	 * 	<Summary>
	 * 	Takes a Point3D cube position and creates a new array in each bomb list for a new active bomb
	 * 	</Summary>
	 */
	public void SetBomb(Point3D cubePos){
		bc.SetBomb (cubePos);
	}

	/*
	 * <Summary>
	 * An active bomb has moved, we need to update the position in the bomb list
	 * </Summary>
	 */
	public void UpdateBomb(Point3D oldPos, Point3D newPos){
		bc.UpdateBomb (oldPos, newPos);
	}

	public bool PositionIsFree(Point3D cubePos){
		return world.PositionIsFree(cubePos);
	}
}
