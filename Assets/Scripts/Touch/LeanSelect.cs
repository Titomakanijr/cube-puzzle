using UnityEngine;

namespace Lean.Touch
{
	// This script allows you to select a GameObject using any finger, as long it has a collider
	public class LeanSelect: MonoBehaviour
	{
		[Tooltip("This stores the layers we want the raycast to hit (make sure this GameObject's layer is included!)")]
		public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;

		//Cube object which will be used when a cube is tapped on screen
		public static bool isFingerDown;

		Cube tapCube;
		Player player;
		WorldController world;
		LevelController levelCont;
		TouchCameraController tcCont;

		void Start(){
			player = FindObjectOfType<Player> ();
			levelCont = FindObjectOfType<LevelController> ();
			tcCont = FindObjectOfType<TouchCameraController> ();
			isFingerDown = false;
		}

		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerDown += OnFingerDown;
			LeanTouch.OnFingerUp += OnFingerUp;
			LeanTouch.OnFingerTap += OnFingerTap;
		}
		
		protected virtual void OnDisable()
		{
			// Unhook the events
			LeanTouch.OnFingerDown -= OnFingerDown;
			LeanTouch.OnFingerUp -= OnFingerUp;
			LeanTouch.OnFingerTap -= OnFingerTap;
		}
		
		public void OnFingerTap(LeanFinger finger)
		{
			// Raycast information
			var ray = finger.GetRay ();
			var hit = default(RaycastHit);
			
			// Was this finger pressed down on a collider?
			if (Physics.Raycast (ray, out hit, float.PositiveInfinity, LayerMask) == true) {
				// Make sure we tapped a cube
				if (hit.collider.gameObject.tag == "Cube") {
					tapCube = hit.collider.gameObject.GetComponent<Cube> ();
					Vector3 playerWaypoint = player.GetNextWaypoint ();
					Point3D playerPos = new Point3D (playerWaypoint.x, playerWaypoint.y, playerWaypoint.z);
					Point3D cubePos = new Point3D (tapCube.transform.position.x, tapCube.transform.position.y + 1, tapCube.transform.position.z);
					if(playerPos != cubePos){
						player.UpdatePathFindingNode (PathFinder.FindPath (player.GetWorld(), playerPos, cubePos), cubePos);
					}
				} else if (hit.collider.gameObject.tag == "MenuItem") {
					SwipeMenuItem item = hit.collider.GetComponent<SwipeMenuItem> ();
		
					if (item.parMenu.IsCenter (item)) {
						//CODE FOR SELECTING CURRENT SHOP ITEM HERE
					} else {
						item.parMenu.CenterItem (item);
					}
					//The player has selected a level
				} else if (hit.collider.gameObject.tag == "LevelSelect") {
					if (tcCont == null) {
						tcCont = FindObjectOfType<TouchCameraController> ();
					}
					LevelSelectObject tapLevel = hit.collider.GetComponent<LevelSelectObject> ();
					//Move the camera to the level select objects y value
					tcCont.LerpTo (tapLevel.transform.position.y);
					//If this level object is already active we should load the level
					if (tapLevel.GetActive ()) {
						levelCont.LoadLevel ();
					}else {
						//Check if this level is currently unlocked
						if (tapLevel.GetLevelNumber () <= levelCont.GetHighestUnlocked()) {
							//Move the player to this level select object
							Vector3 playerWaypoint = player.GetNextWaypoint ();
							Point3D playerPos = new Point3D (playerWaypoint.x, playerWaypoint.y, playerWaypoint.z);
							Point3D cubePos = new Point3D (tapLevel.transform.position.x, tapLevel.transform.position.y, tapLevel.transform.position.z);
							player.UpdatePathFindingNode (PathFinder.FindPath (player.GetWorld(), playerPos, cubePos), cubePos);
							//Set this levelObject as the currently selected level object
							levelCont.SetLevelObject(tapLevel);
						}
					}
				}
			}
		}

		public void OnFingerDown(LeanFinger finger){
			isFingerDown = true;
			// Raycast information
			var ray = finger.GetRay ();
			var hit = default(RaycastHit);

			// Was this finger pressed down on a collider?
			if (Physics.Raycast (ray, out hit, float.PositiveInfinity, LayerMask) == true) {
				// Make sure we tapped a cube
				if (hit.collider.gameObject.tag == "Cube") {
					tapCube = hit.collider.gameObject.GetComponent<Cube> ();
					tapCube.GetComponentInChildren<Renderer> ().material.SetColor ("_Color", new Color(.7f, 1f, 1f));
				} 
			}
		}

		public void OnFingerUp(LeanFinger finger){
			isFingerDown = false;
		}
	}
}