using UnityEngine;

namespace Lean.Touch
{
	public class LeanSwipeRigidbody3D : MonoBehaviour
	{
		// This stores the layers we want the raycast to hit (make sure this GameObject's layer is included!)
		public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;
		
		// This allows use to set how powerful the swipe will be
		public float ForceMultiplier = 1.0f;
		
		Cube pushCube;
		public Player player;
		WorldController worldCon;

		void Start(){
			player = FindObjectOfType<Player> ();
			worldCon = FindObjectOfType<WorldController> ();
		}
		
		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerSwipe += OnFingerSwipe;
		}
		
		protected virtual void OnDisable()
		{
			// Unhook the events
			LeanTouch.OnFingerSwipe -= OnFingerSwipe;
		}
		
		public void OnFingerSwipe(LeanFinger finger)
		{
			// Raycast information
			var ray = finger.GetStartRay();
			var hit = default(RaycastHit);
			var swipe = finger.SwipeScreenDelta;
			// Was this finger pressed down on a collider?
			if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
			{

				if (swipe.x < -Mathf.Abs (swipe.y)) {
					if (player.transform.position.y % 1 == .5f) {
						player.AttemptLedgeMovement (true);
					}
				} else if (swipe.x > Mathf.Abs (swipe.y)) {
					if (player.transform.position.y % 1 == .5f) {
						player.AttemptLedgeMovement (false);
					}
				}
				// Was that collider a cube?
				if (hit.collider.gameObject.tag == "Cube") {
					Point3D cubeCheck;
					pushCube = hit.collider.gameObject.GetComponent<Cube> ();
					//The player and cube are not on the same level
					if (!Mathf.Approximately(pushCube.transform.position.y, player.transform.position.y) && pushCube.transform.position + new Vector3 (0, 1, 0) != player.transform.position) {
						if (!(swipe.y < -Mathf.Abs (swipe.x))) {
							return;
						}
					}
					//Swipe Left
					if (swipe.x < -Mathf.Abs (swipe.y)) {
						if (pushCube.transform.position + new Vector3 (0, 1, 0) == player.transform.position) {
							player.AttemptDropToEdge ("left");
						} else if (!Mathf.Approximately(pushCube.transform.position.z, player.transform.position.z) || player.GetFalling()) {
							return;
						}
						//Figure out 
						int dir = Mathf.Approximately(player.transform.position.x + 1, pushCube.transform.position.x) ? 1 : -1;
						cubeCheck = new Point3D (player.transform.position.x + (1 * dir), player.transform.position.y, player.transform.position.z);
						Point3D movePos = new Point3D (player.transform.position.x - (1 * dir), player.transform.position.y, player.transform.position.z);
						if (Mathf.Approximately(pushCube.transform.position.x, cubeCheck.X)) {
							if (dir == 1 && worldCon.world.PositionIsFree (movePos)) {
								if (pushCube.PushCube (-1, true)) {
									player.UpdatePushNode (new SearchNode (movePos, 0, 0, null), dir, true);
								}
							} else if (dir == -1) {
								pushCube.PushCube (-1, true);
							}
						}
					}
					//Swipe Right
					if (swipe.x > Mathf.Abs (swipe.y)) {
						if (pushCube.transform.position + new Vector3 (0, 1, 0) == player.transform.position) {
							player.AttemptDropToEdge ("right");
						}
						else if (!Mathf.Approximately(pushCube.transform.position.z, player.transform.position.z) || player.GetFalling()) {
							return;
						}
						int dir = Mathf.Approximately(player.transform.position.x + 1, pushCube.transform.position.x) ? 1 : -1;
						cubeCheck = new Point3D (player.transform.position.x + (1 * dir), player.transform.position.y, player.transform.position.z);
						Point3D movePos = new Point3D (player.transform.position.x - (1 * dir), player.transform.position.y, player.transform.position.z);
						if (Mathf.Approximately(pushCube.transform.position.x, cubeCheck.X)) {
							if (dir == -1 && worldCon.world.PositionIsFree (movePos)) {
								if (pushCube.PushCube (1, true)) {
									player.UpdatePushNode (new SearchNode (movePos, 0, 0, null), dir, true);
								}
							} else if (dir == 1) {
								pushCube.PushCube (1, true);
							}
						}
					}
					//Swipe Down
					if (swipe.y < -Mathf.Abs (swipe.x)) {
						//Check if player is standing on this cube
						if (pushCube.transform.position + new Vector3 (0, 1, 0) == player.transform.position || player.transform.position.y % 1 == .5f) {
							if (player.transform.position.y % 1 == .5f) {
								player.AttemptFallToEdge ();
							} else {
								player.AttemptDropToEdge ("front");

							}
						} else if (!Mathf.Approximately(pushCube.transform.position.y, player.transform.position.y) || player.GetFalling()) { //Otherwise if player and cube are not on same level return
							return;
						}else{
							if (!Mathf.Approximately(pushCube.transform.position.x, player.transform.position.x)) {
								return;
							}
							int dir = Mathf.Approximately(player.transform.position.z + 1, pushCube.transform.position.z) ? 1 : -1;
							cubeCheck = new Point3D (player.transform.position.x, player.transform.position.y, player.transform.position.z + (1 * dir));
							Point3D movePos = new Point3D (player.transform.position.x, player.transform.position.y, player.transform.position.z - (1 * dir));
							if (Mathf.Approximately(pushCube.transform.position.z, cubeCheck.Z)) {
								if (dir == 1 && worldCon.world.PositionIsFree (movePos)) {
									if (pushCube.PushCube (-1, false)) {
										player.UpdatePushNode (new SearchNode (movePos, 0, 0, null), dir, false);
									}
								} else if (dir == -1) {
									pushCube.PushCube (-1, false);
								}
							}
						}
					}
					//Swipe Up
					if (swipe.y > Mathf.Abs (swipe.x)) {
						if (pushCube.transform.position + new Vector3 (0, 1, 0) == player.transform.position) {
							player.AttemptDropToEdge ("back");
						} else if (!Mathf.Approximately(pushCube.transform.position.x, player.transform.position.x) || player.GetFalling()) {
							return;
						}
						int dir = Mathf.Approximately(player.transform.position.z + 1, pushCube.transform.position.z) ? 1 : -1;
						cubeCheck = new Point3D (player.transform.position.x, player.transform.position.y, player.transform.position.z + (1 * dir));
						Point3D movePos = new Point3D (player.transform.position.x, player.transform.position.y, player.transform.position.z - (1 * dir));
						if (Mathf.Approximately(pushCube.transform.position.z, cubeCheck.Z)) {
							if (dir == -1 && worldCon.world.PositionIsFree (movePos)) {
								if (pushCube.PushCube (1, false)) {
									player.UpdatePushNode (new SearchNode (movePos, 0, 0, null), dir, false);
								}
							} else if (dir == 1) {
								pushCube.PushCube (1, false);
							}
						}
					}
					//Turn the player towards the cube they are trying to push
					player.SetDirection (player.transform.position, pushCube.transform.position);
				}
			}
		}
	}
}