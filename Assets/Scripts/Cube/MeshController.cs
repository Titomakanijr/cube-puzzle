﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshController : MonoBehaviour {
	public bool blink;
	public Voxelizer vox;

	//private variables
	float interval = 0;
	bool active;
	bool up;
	bool playerExplode;
	string type;
	Color blinkColor = new Color(1, .4f, .4f);
	Cube parCube;
	WorldController worldCont;
	MeshController bombNum = null;
	Renderer rend;
	Material mat;
	Player player;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
		mat = rend.material;
		parCube = GetComponentInParent<Cube> ();
		type = parCube.type;
		if (type != "Unmovable" && type != "Finish") {
			vox = FindObjectOfType<Voxelizer> ();
		}
		worldCont = FindObjectOfType<WorldController> ();
		player = FindObjectOfType<Player> ();
		int dir = Random.Range (0, 4);
		transform.localEulerAngles = new Vector3 (0, 90 * dir, 0);
	}
		
	void Update(){
		//Check if the cube needs to be blinking
		if (blink) {
			Blink ();
			blink = false;
		} else {
			if (mat.GetColor ("_Color") != Color.white && !Lean.Touch.LeanSelect.isFingerDown) {
				mat.SetColor ("_Color", Color.white);
			}
		}
	}


	 /// <summary>
	 /// This function is called when a bomb cube explodes in the vicinity
	 /// This updates certain cube types, and checks to make sure that no new cubes should be falling
	 /// </summary>
	 /// <param name="bn">Bn.</param>
	public void Explode(MeshController bn){
		Point3D parPos = new Point3D (parCube.transform.position.x, parCube.transform.position.y, parCube.transform.position.z);
		Vector3 playerWaypoint = player.GetNextWaypoint ();
		Point3D playerPos = new Point3D (playerWaypoint.x,  playerWaypoint.y,  playerWaypoint.z);
		if (type == "Bomb") {
			if (active && bn == bombNum) {
				vox.BombExplode (parCube.gameObject);
				playerExplode = false;
				//Remove the exploded bomb from the world
				worldCont.RemoveCube (new Point3D (parCube.transform.position.x, parCube.transform.position.y, parCube.transform.position.z));
				worldCont.world.CheckUpperCubes (parPos);
				//Check if there was a player near the bomb
				Collider[] col = Physics.OverlapBox (parCube.transform.position, new Vector3 (1.4f, 1.4f, 1.4f));
				for (int i = 0; i < col.Length; i++){
					if (col [i].gameObject.name == "Player") {
						playerExplode = true;
					}
				}
				if (playerExplode) {
					//The player was within the bomb radius
					player.Die ("bomb", parCube.transform.position.x, parCube.transform.position.y, parCube.transform.position.z);
				} else if (player.HasPathNode ()) {
					//The player was not, update the path node
					player.UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerPos, player.GetEndCube ()), player.GetEndCube ());
					player.CheckEdgeFalling ();
				} else {
					player.CheckEdgeFalling ();
				}
				foreach (Collider coll in Physics.OverlapSphere(parCube.transform.position, 1.5f)) {
					if (coll.attachedRigidbody != null) {
						coll.attachedRigidbody.AddExplosionForce(500, parCube.transform.position, 10f, 0, ForceMode.Force);
					}
				}
				//Destroy the parent cube
				Destroy (transform.parent.gameObject);
			} else if(!active){
				parCube.SetActive ();
			}
		} else {
			if (type != "Unmovable" && type != "Finish") {
				if (type == "Cracked") {
					vox.CrackedBreak (transform.parent.gameObject);
					worldCont.RemoveCube (parPos, true);
					Destroy (transform.parent.gameObject);
					if (player.HasPathNode ()) {
						player.UpdatePathFindingNode(PathFinder.FindPath(worldCont.world, playerPos, player.GetEndCube()), player.GetEndCube());
						player.CheckEdgeFalling ();
					}
				} else if (type == "") {
					parCube.type = "Cracked";
					parCube.UpdateMesh();
					type = "Cracked";
					bombNum = null;
				}
			}
		}
	}
		
	/*
	 * <Summary>
	 * Returns whether the cube is falling or has been pushed
	 * </Summary>
	 */
	public bool GetFallingOrPushed(){
		if (parCube.falling || parCube.pushed) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * <Summary>
	 * This function is the setter for the blink function attached to this cube mesh
	 * </Summary>
	 */
	public void StartBlink(float newInterval, MeshController bn){
		if (bombNum == null || (type == "Bomb" && active == true)) {
			bombNum = bn;
		} else if (bombNum != bn) {
			return;
		}
		blink = true;
		interval = newInterval;
	}
		
	public void SetActive(){
		active = true;
	}

	public bool GetActive(){
		return active;
	}

	public string GetMeshType(){
		return type;
	}
		
	/*
	 * <Summary>
	 * This function will lerp between black and red color emmision for the cube shader
	 * Essentially making the cube blink
	 * </Summary>
	 */ 
	void Blink(){
		//Set the material color
		Color finalColor = Color.Lerp (Color.white, blinkColor, interval);
		mat.SetColor ("_Color", finalColor);
	}
}
