﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {
	//public Variables

	public bool bottom;
	public bool pushed = false;
	public bool falling = false;
	public string type;

	//private variables
	float fallTime = .85f;
	float fallTimer;
	float moveSpeed = 5f;
	float stepCounter = 0;
	float shakeAmount = 4f;
	float shakeSpeed = 60f;
	float bobAmount = .04f;
	float bobTime = .04f;
	float bobTimer;
	float breakTime = .75f;
	float breakTimer;
	int pushDir;
	bool horiz = false;
	bool moving = false;
	bool belowLevel = false;
	bool initialEdgeCheck = false;
	bool active = false;
	bool cubeDown = false;
	bool cubeUp = false;
	MovementController moveCont;
	WorldController worldCont;
	Transform myTransform;
	MeshController myMeshCont;

	// Use this for initialization
	void Start () {
		fallTimer = fallTime;
		myMeshCont = GetComponentInChildren<MeshController> ();
		moveCont = gameObject.GetComponent<MovementController> ();
		myTransform = transform;
		moveCont.globalWaypoints [1] = myTransform.position;
		worldCont = FindObjectOfType<WorldController> ();
		worldCont.AddCube (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z)); 
	}

	void FixedUpdate(){
		if (!initialEdgeCheck) {
			if (!bottom) {
				worldCont.world.CheckLowerCubes (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z));
			}
			initialEdgeCheck = true;
		} 
	}

	// Update is called once per frame
	void Update () {
		if (type == "Cracked" && stepCounter >= 2) {
			if (breakTimer >= 0) {
				breakTimer -= Time.deltaTime;
			} else {
				worldCont.RemoveCube (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z));
				myMeshCont.vox.CrackedBreak (gameObject);
				Destroy (gameObject);
			}
		}
		//The block was pushed by the player
		if (pushed) {
			if (!moving) {
				moving = true;
				//Set up our waypoints for moving on the x or z plane
				moveCont.globalWaypoints [0] = myTransform.position;
				if (horiz) {
					moveCont.globalWaypoints [1] = new Vector3 (myTransform.position.x + 1 * pushDir, myTransform.position.y, myTransform.position.z);
				} else {
					moveCont.globalWaypoints [1] = new Vector3 (myTransform.position.x, myTransform.position.y, myTransform.position.z + 1 * pushDir);
				}
			}
			fallTimer -= Time.deltaTime;
			Vector3 velocity = moveCont.CalculateMovement (moveSpeed);
			myTransform.Translate (velocity, Space.World);
			//The cube has reached the waypoint
			if (myTransform.position == moveCont.globalWaypoints [1]) {
				pushed = false;
				moving = false;
				Point3D newPos = new Point3D(myTransform.position.x, myTransform.position.y, myTransform.position.z);
				Point3D oldPos = new Point3D(moveCont.globalWaypoints [0].x, moveCont.globalWaypoints [0].y, moveCont.globalWaypoints [0].z);
				//Check if this cube is resting on anything
				worldCont.world.CheckLowerCubes (newPos);
				if (!falling) { //Cube was resting on another
					worldCont.world.CheckUpperCubes (newPos);
				}
				//Check the cubes around the old position to check if any are now falling
				worldCont.world.CheckUpperCubes (oldPos);
				if (type == "Bomb" && active) {
					worldCont.UpdateBomb (oldPos, newPos);
				}
			}
		} else if (falling) { //The cube is currently in a falling state
			//Current position for the cube
			//The cube hasn't started falling yet
			if (fallTimer > 0) {
				fallTimer -= Time.deltaTime;
				//Shake the cube to show it is about to fall
				transform.localEulerAngles = new Vector3 (0, 0, -Mathf.Sin (Time.time * shakeSpeed) * shakeAmount);
				if (moveCont.globalWaypoints [1] == myTransform.position) {
					Point3D cubePos = new Point3D ((int)myTransform.position.x, (int)myTransform.position.y, (int)myTransform.position.z);
					moveCont.globalWaypoints [0] = myTransform.position;
					moveCont.globalWaypoints [1] = new Vector3 (myTransform.position.x, myTransform.position.y - 1, myTransform.position.z);
					worldCont.RemoveCube (cubePos); 
				}
			} else {
				if (myTransform.localEulerAngles != Vector3.zero) {
					myTransform.localEulerAngles = Vector3.zero;
				}
				Vector3 velocity = moveCont.CalculateMovement (moveSpeed);
				myTransform.Translate (velocity, Space.World);
				if (myTransform.position == moveCont.globalWaypoints [1]) {
					if (myTransform.position.y < 0) {
						if (myTransform.position.y < -20) {
							Destroy (gameObject);
						}
						belowLevel = true;
					}
					if (!belowLevel) {
						Point3D cubePos = new Point3D ((int)myTransform.position.x, (int)myTransform.position.y, (int)myTransform.position.z);
						worldCont.world.CheckLowerCubes (cubePos);
						if (type == "Bomb" && active) {
							Point3D newPos = new Point3D(myTransform.position.x, myTransform.position.y, myTransform.position.z);
							Point3D oldPos = new Point3D(moveCont.globalWaypoints [0].x, moveCont.globalWaypoints [0].y, moveCont.globalWaypoints [0].z);
							worldCont.UpdateBomb (oldPos, newPos);
						}
					}
					if(falling){
						moveCont.globalWaypoints [0] = myTransform.position;
						moveCont.globalWaypoints [1] = new Vector3 (myTransform.position.x, myTransform.position.y - 1, myTransform.position.z);
					} else {
						ResetFall ();
						worldCont.AddCube(new Point3D((int)myTransform.position.x, (int)myTransform.position.y, (int)myTransform.position.z));
					}
				}
			}
		}
		//If the cube is not falling it's rotation should be (0,0,0)
		if (!falling) {
			if (myTransform.localEulerAngles != Vector3.zero) {
				myTransform.localEulerAngles = Vector3.zero;
			}
		}
		if (cubeDown) {
			myTransform.Translate(new Vector3(0, -bobAmount, 0));
			cubeDown = false;
			cubeUp = true;
			bobTimer = bobTime;
		} else if (cubeUp) {
			if (bobTimer > 0) {
				bobTimer -= Time.deltaTime;
			} else {
				myTransform.Translate (new Vector3 (0, bobAmount, 0));
				cubeUp = false;
			}
		}
	}

	/*
	 * <Summary>
	 * recursively checks if the cube can be pushed in the direction we were passed in, returns true if pushed
	 * </Summary>
	 */
	public bool PushCube(int dir, bool horizontal){
		if (type == "Unmovable" || type == "Finish") {
			return false;
		}
		Collider[] col;
		Cube adjCube;
		if (horizontal) {
			//Check for cube next to pushed cube
			if (!worldCont.PositionIsFree (new Point3D (myTransform.position.x + (1 * dir), myTransform.position.y, myTransform.position.z))) {
				col = Physics.OverlapSphere (new Vector3(myTransform.position.x + (1 * dir), myTransform.position.y, myTransform.position.z), .1f);
				adjCube = col[0].GetComponent<Cube> ();
				//Check if the found cube can also be pushed
				if (!adjCube.PushCube (dir, horizontal)) {
					return false;
				}
			}
		} else {
			if (!worldCont.PositionIsFree (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z + (1 * dir)))) {
				col = Physics.OverlapSphere (new Vector3(myTransform.position.x, myTransform.position.y, myTransform.position.z + (1 * dir)), .1f);
				adjCube = col[0].GetComponent<Cube> ();
				//Check if the found cube can also be pushed
				if (!adjCube.PushCube (dir, horizontal)) {
					return false;
				}
			}
		}
		//Remove the cube from the world, we do this here so there are no problems when adding them back in with the update function
		worldCont.RemoveCube (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z));
		pushDir = dir;
		horiz = horizontal;
		pushed = true;
		return true;
	}

	//Takes a bool which will set if the cube should be falling or not
	public void SetFalling(bool fall){
		if (!bottom) {
			falling = fall;
		}
	}

	public void SetActive(){
		if (!active) {
			active = true;
			if (type == "Bomb") {
				worldCont.SetBomb (new Point3D (myTransform.position.x, myTransform.position.y, myTransform.position.z));
			}

		}
	}

	public void SteppedOn(){
		stepCounter++;
		myMeshCont.vox.CrackedStep (gameObject);
		if (stepCounter == 2) {
			breakTimer = breakTime;
		} 
	}

	public void Bob(){
		cubeDown = true;
	}

	public void ResetFall(){
		fallTimer = fallTime;
		moveCont.globalWaypoints [1] = myTransform.position;
	}

	public void UpdateMesh(){
		//UPDATE MESH CODE, if now cracked set vox object
	}
}
