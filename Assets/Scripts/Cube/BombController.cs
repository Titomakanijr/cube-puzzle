﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class BombController : MonoBehaviour {

	float blinkTime = 5f;
	List<float> blinkT = new List<float>();
	List<float> interval = new List<float> ();
	List<bool> up = new List<bool>();
	List<Point3D> bombs = new List<Point3D>();
	List<MeshController> bombMeshes = new List<MeshController>();
	List<MeshController[]> surCubes = new List<MeshController[]>();
	Collider col;
	MeshController curCube;
	WorldController worldCont;

	void Start(){
		worldCont = GetComponent<WorldController> ();
	}

	void LateUpdate(){
		for (int i = 0; i < bombs.Count; i++) {
			//Bomb timer has gone below 0, we need to explode
			if (blinkT[i] < 0) {
				bombMeshes [i].Explode (bombMeshes[i]);
				for (int j = 0; j < surCubes [i].Length; j++) {
					if (surCubes [i] [j] != null) {
						surCubes [i] [j].Explode (bombMeshes[i]);
					}
				}
				//Clear this spot in the list
				bombs.RemoveAt (i);
				bombMeshes.RemoveAt (i);
				blinkT.RemoveAt (i);
				interval.RemoveAt (i);
				surCubes.RemoveAt (i);
				continue;
			}else if (blinkT[i] < 1f) {
				if (up[i]) {
					interval[i] = Mathf.Lerp (interval[i], 1, .13f);
					if (interval[i] > .9f) {
						up[i] = false;
					}
				} else { //Second stage, slightly faster
					interval[i] = Mathf.Lerp (interval[i], 0, .13f);
					if (interval[i] < .1f) {
						up[i] = true;
					}
				}
			} else if (blinkT[i] < 3f) {  //Second stage, slightly faster
				if (up[i]) {
					interval[i] = Mathf.Lerp (interval[i], 1, .08f);
					if (interval[i] > .9f) {
						up[i] = false;
					}
				} else {
					interval[i] = Mathf.Lerp (interval[i], 0, .08f);
					if (interval[i] < .1f) {
						up[i] = true;
					}
				}
			} else { //Initial blink speed, slowest
				if (up[i]) {
					interval[i] = Mathf.Lerp (interval[i], 1, .03f);
					if (interval[i] > .9f) {
						up[i] = false;
					}
				} else {
					interval[i] = Mathf.Lerp (interval[i], 0, .03f);
					if (interval[i] < .1f) {
						up[i] = true;
					}
				}
			}
			//update blink timer
			blinkT[i] -= Time.deltaTime;
			bombMeshes[i].StartBlink (interval[i], bombMeshes[i]);
			//Check each surrounding cube to see if it should be blinking
			for (int j = 0; j < bombSurrounding.Length; j++) {
				Point3D curCubePos = new Point3D (bombs [i], bombSurrounding [j]);
				//The position is not free
				if (!(worldCont.PositionIsFree (curCubePos))) {
					//this spot in the array is open so we add the MeshController
					if (surCubes[i][j] == null) {
						col = Physics.OverlapSphere (new Vector3 (curCubePos.X, curCubePos.Y, curCubePos.Z), .25f).First ();
						surCubes[i][j] = col.gameObject.GetComponentInChildren<MeshController> ();
					}
					//If this is another active bomb we want to ignore it
					if (surCubes [i] [j].GetMeshType() == "Bomb" && surCubes[i][j].GetActive() == true) {
						continue;
					}
					surCubes [i] [j].StartBlink (interval [i], bombMeshes[i]);
					//This spot in the array is taken, but there is no cube in the world
				} else if (surCubes[i][j] != null){
					//If the cube is currently 
					if (surCubes[i][j].GetFallingOrPushed ()) {
						if (surCubes [i] [j].GetMeshType() == "Bomb" && surCubes[i][j].GetActive() == true) {
							continue;
						}
						surCubes[i][j].StartBlink (interval [i], bombMeshes[i]);
					} else {
						surCubes[i][j] = null;
					}
				}
			}
		}
	}

	/*
	 * 	<Summary>
	 * 	Takes a Point3D cube position and creates a new array in each bomb list for a new active bomb
	 * 	</Summary>
	 */
	public void SetBomb(Point3D cubePos){
		bombs.Add (cubePos);
		blinkT.Add (blinkTime);
		interval.Add (0);
		surCubes.Add (new MeshController[26]);
		col = Physics.OverlapSphere (new Vector3 (cubePos.X, cubePos.Y, cubePos.Z), .25f).First();
		bombMeshes.Add(col.gameObject.GetComponentInChildren<MeshController>());
		bombMeshes.Last ().SetActive ();
		up.Add (true);
	}

	/*
	 * <Summary>
	 * An active bomb has moved, we need to update the position in the bomb list
	 * </Summary>
	 */
	public void UpdateBomb(Point3D oldPos, Point3D newPos){
		for(int i = 0; i < bombs.Count; i++){
			if (bombs [i] == oldPos) {
				bombs [i] = newPos;
				for (int j = 0; j < surCubes [i].Length; j++) {
					surCubes [i] [j] = null;
				}
			}
		}
	}

	private static Point3D[] bombSurrounding = new Point3D[]{
		//Top slice (Y=1)
		new Point3D(0,1,0), new Point3D(0,1,1), 
		new Point3D(-1,1,0), new Point3D(1,1,0),
		new Point3D(0,1,-1), new Point3D(1,1,-1),
		new Point3D(-1,1,-1), new Point3D(1,1,1),
		new Point3D(-1,1,1),
		//Middle slice (Y = 0)
		new Point3D(0,0,1), new Point3D(-1,0,1),
		new Point3D(-1,0,0), new Point3D(1,0,0),
		new Point3D(0,0,-1), new Point3D(1,0,-1),
		new Point3D(-1,0,-1), new Point3D(1,0,1),
		//Bottom slice (Y = -1)
		new Point3D(0,-1,0), new Point3D(0,-1,1), 
		new Point3D(-1,-1,0), new Point3D(1,-1,0),
		new Point3D(0,-1,-1), new Point3D(1,-1,-1),
		new Point3D(-1,-1,-1), new Point3D(1,-1,1),
		new Point3D(-1,-1,1),
	};
}
