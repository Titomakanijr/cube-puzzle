﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {
	int highestUnlocked;
	int currentLevel;
	bool menuSetup;
	bool openFinished;

	GameObject touchHandler;
	Player player;
	LevelSelectObject currentLevelSelect;
	TouchCameraController tcCont;
	PersistentMenuUIController perMenuCont;
	DataManager dataMan;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<Player> ();
		perMenuCont = FindObjectOfType<PersistentMenuUIController> ();
		touchHandler = FindObjectOfType<Lean.Touch.LeanTouch> ().gameObject;
		dataMan = GetComponent<DataManager> ();
		highestUnlocked = dataMan.GetHighestLevel ();
		//Make sure the players highest level is within the proper bounds
		if(highestUnlocked <= 0 || highestUnlocked > dataMan.GetMaxLevel()){
			highestUnlocked = 1;
			dataMan.SetHighestLevel (highestUnlocked);
		}
		currentLevel = highestUnlocked;
		menuSetup = true;
	}

	void Update(){
		if(SceneManager.GetActiveScene().name == "Main Menu"){
			if (menuSetup) {
				currentLevelSelect = GameObject.Find ("Level" + highestUnlocked).GetComponent<LevelSelectObject> ();
				currentLevelSelect.SetActive (true);
				player.SetStart (currentLevelSelect.transform.position);
				tcCont = FindObjectOfType<TouchCameraController> ();
				tcCont.SetPosition (currentLevelSelect.transform.position.y);
				menuSetup = false;
			}
		}
	}

	public void SetLevelObject(LevelSelectObject newLevel){
		currentLevelSelect.SetActive (false);
		currentLevelSelect = newLevel;
		currentLevelSelect.SetActive (true);
	}
		
	/// <summary>
	/// Loads the level associated 
	/// </summary>
	public void LoadLevel(){
		//LOAD LEVEL FROM MENU
		perMenuCont.SetLevelUIActive(true);
		perMenuCont.SetSettingsActive (false);
		SceneManager.LoadSceneAsync ("Level " + currentLevelSelect.GetLevelNumber());
	}
		
	public void LoadNext(){
		perMenuCont.SetEndLevelMenuActive(false);
		perMenuCont.SetPauseObject (true);
		SceneManager.LoadSceneAsync ("Level " + (currentLevel + 1));
	}

	public void ReloadLevel(){
		//RELOAD CURRENT LEVEL
		perMenuCont.SetPauseMenu (false);
		if (Time.timeScale != 1) {
			Time.timeScale = 1;
			touchHandler.SetActive (true);
			perMenuCont.SetPauseObject (true);
		}
		perMenuCont.SetEndLevelMenuActive(false);
		SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}

	/// <summary>
	/// Loads the Main Menu Scene
	/// </summary>
	public void LoadMenu(){
		player.SetMenu (true);
		perMenuCont.SetLevelUIActive (false);
		perMenuCont.SetEndLevelMenuActive (false);
		perMenuCont.SetSettingsActive (true);
		//If the menu was loaded from pause menu restart time, enable touch handler and unload pause menu
		if (Time.timeScale != 1) {
			perMenuCont.SetPauseMenu (false);
			touchHandler.SetActive (true);
			Time.timeScale = 1;
		}
		SceneManager.LoadScene("Main Menu");
		//REMOVE THIS WHEN A LOADING SCENE IS ADDED!!!
		player.transform.localEulerAngles = Vector3.zero;
	}

	public int GetHighestUnlocked(){
		return highestUnlocked;
	}

	/// <summary>
	/// The current Highest Level the player is on
	/// </summary>
	public int GetCurrenLevel(){
		return currentLevel;
	}

	public void SetCurrentLevel(int newLevelNum){
		currentLevel = newLevelNum;
	}

	public void FinishLevel(){
		if (currentLevel == highestUnlocked) {
			highestUnlocked++;
			dataMan.SetHighestLevel (highestUnlocked);
		}
		if (openFinished) {
			perMenuCont.SetEndLevelMenuActive (true);
			openFinished = false;
		}
	}

	public void levelInit(){
		openFinished = true;
		menuSetup = true;
	}

	public void PauseLevel(){
		Time.timeScale = 0;
		touchHandler.SetActive (false);
		perMenuCont.SetPauseMenu (true);
	}

	public void UnpauseLevel(){
		Time.timeScale = 1;
		touchHandler.SetActive (true);
		perMenuCont.SetPauseMenu (false);
	}
}

