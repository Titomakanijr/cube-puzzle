﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCameraController : MonoBehaviour {
	public float levelWidth;

	float maxRotate = 9;
	Player player;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<Player> ();
		player.SetCamera (this);
	}
	
	public void UpdateCameraTransform(Vector3 newVelocity){
		transform.Translate (newVelocity, Space.World);
		transform.Rotate (new Vector3 (0, -newVelocity.x * ((maxRotate * 2) / (levelWidth - 1)), 0), Space.World);
	}
}
