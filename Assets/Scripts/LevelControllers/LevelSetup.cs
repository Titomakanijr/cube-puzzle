﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetup : MonoBehaviour {
	public int levelNum;

	bool turnOffMenu;
	LevelController levelCont;
	Player player;

	void Start () {
		player = FindObjectOfType<Player> ();
		player.SetStart (transform.position);
		levelCont = FindObjectOfType<LevelController> ();
		levelCont.levelInit ();
		turnOffMenu = true;
	}
	
	// Update is called once per frame
	void Update () {
		//if we are testing make sure menu is false
		if (turnOffMenu) {
			player.SetMenu (false);
			turnOffMenu = false;
		}
		if (levelCont.GetCurrenLevel () != levelNum) {
			levelCont.SetCurrentLevel (levelNum);
		}
	}
}
