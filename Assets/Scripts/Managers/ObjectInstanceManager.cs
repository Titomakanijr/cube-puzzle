﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInstanceManager: MonoBehaviour {
	public GameObject playerPrefab;
	public GameObject touchPrefab;
	public GameObject perMenuPrefab;

	GameObject selfInstance;
	GameObject temp;

	// Use this for initialization
	void Awake () {
		if (selfInstance == null && FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
			return;
		} else {
			selfInstance = this.gameObject;
		}
		GameObject temp = Instantiate (playerPrefab);
		temp.name = temp.name.Replace ("(Clone)", "");
		temp = Instantiate (perMenuPrefab);
		temp.name = temp.name.Replace ("(Clone)", "");
		temp = Instantiate (touchPrefab);
		temp.name = temp.name.Replace ("(Clone)", "");

	}
}
