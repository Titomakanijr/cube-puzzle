﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {
	public bool resetAllPrefs;
	public bool resetHighestLevel;


	int highestUnlockedLevel;
	int coins;
	int maxLevel = 100;
	string playerHat;
	string playerHead;
	string playerBody;
	GameObject selfInstance;

	void Awake(){
		if (selfInstance == null && FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
			return;
		} else {
			selfInstance = this.gameObject;
		}

		if (resetAllPrefs) {
			PlayerPrefs.DeleteAll ();
		} else if(resetHighestLevel){
			PlayerPrefs.SetInt ("HighestUnlockedLevel", 1);
		}
		highestUnlockedLevel = PlayerPrefs.GetInt ("HighestUnlockedLevel");
		coins = PlayerPrefs.GetInt ("Coins");
		//Make sure player doesn't have a negative amount of coins
		if (coins < 0) {
			coins = 0;
			PlayerPrefs.SetInt ("Coins", coins);
		}
		playerHat = PlayerPrefs.GetString ("PlayerHat");
		playerHead = PlayerPrefs.GetString ("PlayerHead");
		playerBody = PlayerPrefs.GetString ("PlayerBody");
	}

	public void SetHat(string hat){
		playerHat = hat;
		PlayerPrefs.SetString ("PlayerHat", hat);
	}

	public string GetHat(){
		return playerHat;
	}

	public void SetHead(string head){
		playerHead = head;
		PlayerPrefs.SetString ("PlayerHead", head);
	}

	public string GetHead(){
		return playerHead;
	}

	public void SetBody(string body){
		playerBody = body;
		PlayerPrefs.SetString ("PlayerBody", body);
	}

	public string GetBody(){
		return playerBody;
	}

	public void SetHighestLevel(int level){
		highestUnlockedLevel = level;
		PlayerPrefs.SetInt ("HighestUnlockedLevel", level);
	}

	public int GetHighestLevel(){
		return highestUnlockedLevel;
	}

	public void SetCoins(int addCoins){
		coins += addCoins;
		PlayerPrefs.SetInt ("Coins", coins);
	}

	public int GetCoins(){
		return coins;
	}

	public int GetMaxLevel(){
		return maxLevel;
	}
}
