﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * 	<Summary>
 * 	Class that contains a Point3D and the move cost of a surrounding node
 * 	</Summary>
 */
class Surr
{
	public Surr(int x, int y, int z)
	{
		Point = new Point3D(x, y, z);
		Cost = x * x + y * y + z * z;
	}

	public Surr(float x, float y, float z)
	{
		Point = new Point3D(x, y, z);
		Cost = x * x + y * y + z * z;
	}

	public Point3D Point;
	public float Cost;
}
