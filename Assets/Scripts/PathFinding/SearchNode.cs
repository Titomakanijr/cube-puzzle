﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/// <summary>
/// Author: Roy Triesscheijn (http://www.roy-t.nl)
/// Class defining BreadCrumbs used in path finding to mark our routes
/// </summary>
public class SearchNode
{
    public Point3D position;
    public float cost;
    public float pathCost;
    public SearchNode next;
    public SearchNode nextListElem;

    public SearchNode(Point3D position, float cost, float pathCost, SearchNode next)
    {
        this.position = position;
        this.cost = cost;
        this.pathCost = pathCost;
        this.next = next;
    }
}
