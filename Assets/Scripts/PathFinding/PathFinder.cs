using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Author: Roy Triesscheijn (http://www.roy-t.nl)
/// Class providing 3D pathfinding capabilities using A*.
/// Heaviliy optimized for speed therefore uses slightly more memory
/// On rare cases finds the 'almost optimal' path instead of the perfect path
/// this is because we immediately return when we find the exit instead of finishing
/// 'neighbour' loop.
/// </summary>
public class PathFinder : MonoBehaviour
{                   
	/// <summary>
	/// Method that switfly finds the best path from start to end.
	/// </summary>
	/// <returns>The starting breadcrumb traversable via .next to the end or null if there is no path</returns>        
	public static SearchNode FindPath(World world, Point3D start, Point3D end, bool falling = false)
	{
		//note we just flip start and end here so you don't have to.    
		if (!world.PositionIsFree (end)) {
			return null;
		}
		return FindPathReversed(world, end, start, falling); 
	}        

	/// <summary>
	/// Method that switfly finds the best path from start to end. Doesn't reverse outcome
	/// </summary>
	/// <returns>The end breadcrump where each .next is a step back)</returns>
	private static SearchNode FindPathReversed(World world, Point3D start, Point3D end, bool falling)
	{
		SearchNode startNode = new SearchNode(start, 0, 0, null);

		MinHeap openList = new MinHeap();
		openList.Add(startNode);

		float sx = world.Right;
		float sy = world.Top;
		float sz = world.Back;
		bool[] brWorld = new bool[(int)(sx * sy * sz)];
		brWorld[(int)(start.X + (start.Y + start.Z * sy) * sx)] = true;

		while (openList.HasNext())
		{                
			SearchNode current = openList.ExtractFirst();
			if (current.position.Y > world.Top || current.position.X > world.Right 
				|| current.position.Z > world.Back) {
				//return null;
				continue;
			}
			if (current.position == end)
			{
				return current;
			}
			//The current position is right on top of a cube
			if (current.position.Y % 1 == 0) {
				for (int i = 0; i < surroundingCube.Length; i++) {
					Surr surr = surroundingCube [i];
					Point3D tmp = new Point3D (current.position, surr.Point);
					//searching upwards, make sure there are no cubes above
					if (surr.Point.Y == 1
							&& !world.PositionIsFree (new Point3D (current.position.X, current.position.Y + 1, current.position.Z))) {
						continue;
					//Searching .5 below, check if there are cubes next to the player or next to the cube they are standing on
					} else if (surr.Point.Y == -.5
						&& (world.PositionIsFree (new Point3D (current.position.X, current.position.Y - 1, current.position.Z))
							|| !world.PositionIsFree (new Point3D (tmp.X + surr.Point.X, tmp.Y + .5f, tmp.Z + surr.Point.Z))
							|| !world.PositionIsFree (new Point3D (tmp.X + surr.Point.X, tmp.Y - .5f, tmp.Z + surr.Point.Z)))) {
						continue;
					//Searching .5 above, make sure there is a cube we are climbing on and not one currently the current looked at pos
					} else if (surr.Point.Y == .5f
							&& ((world.PositionIsFree (new Point3D (tmp.X + surr.Point.X, tmp.Y - .5f, tmp.Z + surr.Point.Z)))
							|| !world.PositionIsFree(new Point3D (current.position.X, current.position.Y + 1, current.position.Z)))) {
						continue;
					//moving in the 4 main directions on a cube, checks if there is a cube below the temp point, and one below the player
					} else if (surr.Point.Y == 0
						&& (world.PositionIsFree (new Point3D (tmp.X, tmp.Y - 1, tmp.Z))
							|| world.PositionIsFree (new Point3D (current.position.X, current.position.Y - 1, current.position.Z)))) {
						continue;
					}
					//Attempt to add this path
					int brWorldIdx = (int)(tmp.X + (tmp.Y + tmp.Z * sy) * sx);
					if (world.PositionIsFree (tmp) && brWorld [brWorldIdx] == false) {
						brWorld [brWorldIdx] = true;
						float pathCost = current.pathCost + surr.Cost;
						float cost = pathCost + tmp.GetDistanceSquared (end);
						SearchNode node = new SearchNode (tmp, cost, pathCost, current);
						openList.Add (node);
					}
				}
			//We are on the edge of a cube
			} else {
				bool vert = false;
				float dirX = 0;
				float dirZ = 0;
				//We are holding onto the front or back of a cube, get the direction of that cube
				if (current.position.X % 1 == 0) {
					vert = true;
					Point3D cubeCheck = new Point3D (current.position, new Point3D (0, -.5f, .5f));
					dirZ = world.PositionIsFree(cubeCheck) ? -1 : 1;
					if((dirZ == -1 && world.PositionIsFree(new Point3D (current.position, new Point3D (0, -.5f, -.5f)))) 
							|| !world.PositionIsFree(new Point3D(current.position, new Point3D (0, -.5f, .5f * -dirZ)))){
						continue;
					}
					//If we are falling make sure there is not a cube above the cube we are trying to grab the edge of
					if (falling) {
						if(!world.PositionIsFree(new Point3D(current.position.X, current.position.Y + .5f, current.position.Z + (.5f * dirZ)))){
							continue;
						}
					}
				//We are holding onto one of the sides of a cube
				} else {
					Point3D cubeCheck = new Point3D (current.position, new Point3D (.5f, -.5f, 0));
					dirX = world.PositionIsFree(cubeCheck) ? -1 : 1;
					if((dirX == -1 && world.PositionIsFree(new Point3D (current.position, new Point3D (-.5f, -.5f, 0)))) 
							|| !world.PositionIsFree(new Point3D(current.position, new Point3D (.5f * -dirX, -.5f, 0)))){
						continue;
					}
					//If we are falling make sure there is not a cube above the cube we are trying to grab the edge of
					if (falling) {
						if(!world.PositionIsFree(new Point3D(current.position.X + (.5f * dirX), current.position.Y + .5f, current.position.Z))){
							continue;
						}
					}
				}
				//For every possible surrounding path
				for (int i = 0; i < surroundingEdge.Length; i++) {
					Surr surr = surroundingEdge [i];
					Point3D tmp = new Point3D (current.position, surr.Point);
					//holding front or back of cube
					if (vert) {
						if (surr.Point.Y == 0) {
							//We can only move on the x plane from this position
							if (surr.Point.X == 0) {
								continue;
							} else {
								//Check that the cube we are moving to is actually there, and there is not one to the side blocking it
								if (surr.Point.X % 1 == 0) {
									if (!world.PositionIsFree (new Point3D (tmp.X, tmp.Y - .5f, tmp.Z + (.5f * -dirZ)))) {
										continue;
									}
								//Trying to move around the right side of a cube
								} else if (surr.Point.X == .5f) {
									if (dirZ == 1) {
										//moving to the right side of the cube we are holding
										if (surr.Point.Z == .5f 
												&& (!world.PositionIsFree(new Point3D(tmp.X + .5f, tmp.Y - .5f, tmp.Z)) 
												|| !world.PositionIsFree(new Point3D(current.position.X + 1, tmp.Y - .5f, current.position.Z - .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& world.PositionIsFree(new Point3D(current.position.X + 1, tmp.Y - .5f, tmp.Z))) {
											continue;
										}
									//Holding onto the back of the cube
									} else {
										if (surr.Point.Z == .5f 
												&& (world.PositionIsFree(new Point3D(current.position.X  + 1, tmp.Y - .5f, tmp.Z)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& !world.PositionIsFree(new Point3D(current.position.X + 1, tmp.Y - .5f, current.position.Z + .5f))
												|| !world.PositionIsFree(new Point3D(tmp.X + .5f, tmp.Y - .5f, tmp.Z))) {
											continue;
										}
									}
								//Trying to move to the left side of the cube
								} else {
									if (dirZ == 1) {
										if (surr.Point.Z == .5f
												&& (!world.PositionIsFree (new Point3D (tmp.X - .5f, tmp.Y - .5f, tmp.Z))
												|| !world.PositionIsFree (new Point3D (current.position.X - 1, tmp.Y - .5f, current.position.Z - .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& world.PositionIsFree(new Point3D(current.position.X - 1, tmp.Y - .5f, current.position.Z - .5f))) {
											continue;
										}
									} else {
										if (surr.Point.Z == .5f 
												&& (world.PositionIsFree(new Point3D(current.position.X - 1, tmp.Y - .5f, current.position.Z + .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& !world.PositionIsFree(new Point3D(current.position.X - 1, tmp.Y - .5f, current.position.Z + .5f))
												|| !world.PositionIsFree(new Point3D(current.position.X - 1, tmp.Y - .5f, tmp.Z))) {
											continue;
										}
									}
								}
							}
						//Moving up or down .5
						} else {
							//Cant move up or down to the left from this position
							if (surr.Point.X == .5f || surr.Point.X == -.5f) {
								continue;
							}
							if (surr.Point.Y == .5f) {
								if(!world.PositionIsFree(tmp) || !world.PositionIsFree(new Point3D(tmp.X, tmp.Y, tmp.Z - (1 * dirZ)))){
									continue;
								}
								//Check if the direction we are moving is actually on top of the cube we are holding
								if (surr.Point.Z == .5f && dirZ == -1 && !falling) {
									continue;
								} else if (surr.Point.Z == -.5f && dirZ == 1 && !falling) {
									continue;
								}
							} else if(surr.Point.Y == -.5f 
									&& (world.PositionIsFree(new Point3D(tmp.X, tmp.Y - 1, tmp.Z))
									|| !world.PositionIsFree(new Point3D(current.position.X, current.position.Y + .5f, current.position.Z + (.5f * dirZ))) 
									|| !world.PositionIsFree(new Point3D(tmp.X, tmp.Y + 1, tmp.Z)))) {
								continue;
							}
						}
					} else {
						if (surr.Point.Y == 0) {
							if (surr.Point.Z == 0) {
								continue;
							} else {
								if (surr.Point.Z % 1 == 0) { 
									if (!world.PositionIsFree (new Point3D (tmp.X + (.5f * -dirX), tmp.Y - .5f, tmp.Z))) {
										continue;
									}
								//Moving left
								} else if (surr.Point.X == .5f) {
									if (dirX == 1) {
										if (surr.Point.Z == .5f 
												&& (!world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z + .5f)) 
												|| !world.PositionIsFree(new Point3D(current.position.X -.5f, tmp.Y - .5f, tmp.Z + .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
											&& !world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z - .5f)) 
												|| !world.PositionIsFree(new Point3D(current.position.X -.5f, tmp.Y - .5f, tmp.Z - .5f))) {
											continue;
										}
									} else {
										if (surr.Point.Z == .5f 
												&& (world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z + .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
											&& world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z - .5f))) {
											continue;
										}
									}
								} else {
									if (dirX == 1) {
										if (surr.Point.Z == .5f 
												&& world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z  + .5f))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z - .5f))) {
											continue;
										}
									} else {
										if (surr.Point.Z == .5f 
												&& (!world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z + .5f)) 
												|| !world.PositionIsFree(new Point3D(current.position.X + .5f, tmp.Y - .5f, tmp.Z + .5f)))) {
											continue;
										} else if(surr.Point.Z == -.5f 
												&& !world.PositionIsFree(new Point3D(tmp.X, tmp.Y - .5f, tmp.Z - .5f)) 
												|| !world.PositionIsFree(new Point3D(current.position.X + .5f, tmp.Y - .5f, tmp.Z - .5f))) {
											continue;
										}
									}
								}
							}
						} else {
							if (surr.Point.Z == .5f) {
								continue;
							}
							if (surr.Point.Y == .5f) {
								if(!falling && (!world.PositionIsFree(tmp) || !world.PositionIsFree(new Point3D(tmp.X - (1 * dirX), tmp.Y, tmp.Z)))){
									continue;
								}
								if (surr.Point.X == .5f && dirX == -1 && !falling) {
									continue;
								} else if (surr.Point.X == -.5f && dirX == 1 && !falling) {
									continue;
								}
							} else if(surr.Point.Y == -.5f 
									&& (world.PositionIsFree(new Point3D(tmp.X, tmp.Y - 1, tmp.Z))
									|| !world.PositionIsFree(new Point3D(current.position.X + (.5f * dirX), current.position.Y + .5f, current.position.Z))
									|| !world.PositionIsFree(new Point3D(tmp.X, tmp.Y + 1, tmp.Z)))) {
								continue;
							}
						}
					}
					int brWorldIdx = (int)(tmp.X + (tmp.Y + tmp.Z * sy) * sx);
					if (world.PositionIsFree (tmp) && brWorld [brWorldIdx] == false) {
						brWorld [brWorldIdx] = true;
						float pathCost = current.pathCost + surr.Cost;
						float cost = pathCost + tmp.GetDistanceSquared (end);
						SearchNode node = new SearchNode (tmp, cost, pathCost, current);
						openList.Add (node);
					}
				}
			}
		}
		return null; //no path found
	}

	//Neighbour options
	private static Surr[] surroundingCube = new Surr[]{                        
		//Top slice (Y=1)
		new Surr(0, 1, 0),
		//Top edge Slice (Y = .5)
		new Surr(-.5f,.5f,0), new Surr(.5f,.5f,0), 
		new Surr(0,.5f,.5f), new Surr(0,.5f,-.5f), 
		//Bottom edge slice (Y=-.5)
		new Surr(-.5f,-.5f,0), new Surr(.5f,-.5f,0), 
		new Surr(0,-.5f,.5f), new Surr(0,-.5f,-.5f), 
		//Middle slice (Y=0)
		new Surr(0,0,1), new Surr(0,0,-1), 
		new Surr(1,0,0),new Surr(-1,0,0)

	};

	//Neighbour edge options
	private static Surr[] surroundingEdge = new Surr[]{      
		//y = 0, checks surrounding edges
		new Surr(.5f,0,.5f), new Surr(.5f,0,-.5f), 
		new Surr(-.5f,0,.5f),new Surr(-.5f,0,-.5f),
		new Surr(0,0,1), new Surr(0,0,-1), 
		new Surr(1,0,0),new Surr(-1,0,0),
		//y != 0, moves up and down
		new Surr(-.5f,.5f,0), new Surr(.5f,.5f,0), 
		new Surr(0,.5f,.5f), new Surr(0,.5f,-.5f), 
		new Surr(-.5f,-.5f,0), new Surr(.5f,-.5f,0), 
		new Surr(0,-.5f,.5f), new Surr(0,-.5f,-.5f)

	};
}           
