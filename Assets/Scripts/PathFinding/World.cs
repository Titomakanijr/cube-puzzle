﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/// <summary>
/// Author: Roy Triesscheijn (http://www.roy-t.nl)
/// Sample World class that only provides 'is free or not' information on a node
/// </summary>
public class World
{

    private int sx;
	private int sy;
	private int sz;
	private int offsetIdx;
    private bool[] worldBlocked; //extremely simple world where each node can be free or blocked: true=blocked 

    //Note: we use Y as height and Z as depth here!
    public float Left { get { return 0; } }
	public float Right { get { return sx - 2; } }
	public float Bottom { get { return 0; } }
	public float Top { get { return sy - 2; } }
	public float Front { get { return 0; } }
	public float Back { get { return sz - 2; } }


    /// <summary>
    /// Creates a 2D world
    /// </summary>        
    public World(int width, int height)
        : this(width, height, 1)
    { }

    /// <summary>
    /// Creates a 3D world
    /// </summary>        
    public World(int width, int height, int depth)
    {
        // add 2 to compensate for the solid border around the world
        sx = width + 2;
        sy = height + 2;
        sz = depth + 2;
        offsetIdx = (0 + 1) + ((0 + 1) + (0 + 1) * sy) * sx;
		worldBlocked = new Boolean[(int)(sx * sy * sz)];
    }
		

    /// <summary>
    /// Mark positions in the world als blocked (true) or unblocked (false)
    /// </summary>
    /// <param name="value">use true if you wan't to block the value</param>
    public void MarkPosition(Point3D position, bool value)
    {
		int index = Mathf.RoundToInt((offsetIdx + position.X + (position.Y + position.Z * sy) * sx));
		if (index == 3624) {
			index = 5;
		}
		worldBlocked[Mathf.RoundToInt(offsetIdx + position.X + (position.Y + position.Z * sy) * sx)] = value;
    }

    private void MarkPositionEx(Point3D position, bool value)
    {
		worldBlocked[(int)(position.X + (position.Y + position.Z * sy) * sx)] = value;
    }

    /// <summary>
    /// Checks if a position is free or marked (and legal)
    /// </summary>        
    /// <returns>true if the position is free</returns>
    public bool PositionIsFree(Point3D position)
	{
		return !worldBlocked[Mathf.RoundToInt(offsetIdx + position.X + (position.Y + position.Z * sy) * sx)];
	}

	/*
	 * 	<Summary>
	 * 	Function takes in the position of a cube in the world, finds all sorrounding cubes
	 * 	above and checks them for edges
	 * 	</Summary>
	 */
	public void CheckUpperCubes(Point3D cubePos, bool exploded = false){
		for (int i = 0; i < surroundingTop.Length; i++) {
			//Physics Overlap will throw an error if there isn't a game object, we want to catch this and continue
			try{
				Point3D temp = new Point3D(cubePos, surroundingTop[i]);
				Collider col = Physics.OverlapSphere (new Vector3 (temp.X, temp.Y, temp.Z), .25f).First();
				Cube cube = col.gameObject.GetComponent<Cube> ();
				if (cube.transform.position.y % 1 == 0) {
					CheckLowerCubes (temp, exploded);
				} else {
					continue;
				}
			}catch {
				//There was not a game object in this position, continue
				continue;
			}
		}
	}

	/*
	 *	<Summary>
	 *	Function takes in the position of a cube in the world and checks if there is a cube below
	 *	and then each of the edges. If the cube has nothing holding it up the cube is set to falling
	 *	</Summary>
	 */
	public void CheckLowerCubes(Point3D cubePos, bool exploded = false){
		Collider col = Physics.OverlapSphere (new Vector3 (cubePos.X, cubePos.Y, cubePos.Z), .25f).First();
		Cube cube = col.gameObject.GetComponent<Cube> ();
		cube.SetFalling (true);
		for (int i = 0; i < surroundingBot.Length; i++) {
			Point3D tmp = new Point3D (cubePos, surroundingBot [i]);
			if (!(PositionIsFree (tmp))) {
				if (cube.falling) {
					cube.ResetFall ();
				}
				if (i == 0) {
					cube.SetFalling(false);
				} else { //There is no cube below this cube but it is on an edge
					cube.SetFalling(false);
					//CODE FOR TELLING THE CUBE WHICH EDGES IT CAUGHT GOES HERE
				}
				//If the position was free this means the cube was in a falling state
				//but another was pushed to its edge. Re-add and check it's upper cubes
				if (PositionIsFree (cubePos) && !exploded) {
					MarkPosition (cubePos, true);
					CheckUpperCubes (cubePos);
				}
			} else {
				continue;
			}
		}
	}

	//Struct that will be used to get each cube in the surrounding area above a cube
	private static Point3D[] surroundingTop = new Point3D[]{                        
		//Top slice (Y=1)
		new Point3D(0,1,0), new Point3D(0,1,1), 
		new Point3D(-1,1,0), new Point3D(1,1,0),
		new Point3D(0,1,-1),
	};

	//Struct that will be used to get each cube in the surrounding area below a cube
	private static Point3D[] surroundingBot = new Point3D[]{                        
		//Bottom slice (Y=-1)
		new Point3D(0,-1,0), new Point3D(0,-1,1), 
		new Point3D(-1,-1,0), new Point3D(1,-1,0),
		new Point3D(0,-1,-1),
	};
}
