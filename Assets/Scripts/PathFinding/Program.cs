﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.Xna.Framework;

/// <summary>
/// Author: Roy Triesscheijn (http://www.roy-t.nl)
/// Small program that demonstrates the use of the PathFinder class
/// </summary>
class Program : MonoBehaviour
{
    /// <summary>
    /// Small benchmark that creates a 10x10x10 world and then tries to find a path from
    /// 0,0,0 to 9,5,8. Benchmark is run 100 times after which benchmark results and the path are shown        
    /// 1 out 3 nodes in the world are blocked
    /// </summary>        
	void Start()
	{
		double[] bench = new double[20];           
		World world = new World (10, 10, 10);

		for (int x = 0; x < world.Right; x++) {
			for (int y = 0; y < world.Top; y++) {
				for (int z = 0; z < world.Back; z++) {
					//prevent the starting square from being blocked
					if (UnityEngine.Random.Range (0, 2) == 1 && (x + y + z) != 0) {
						world.MarkPosition (new Point3D (x, y, z), true);
						GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
						cube.transform.position = transform.position + new Vector3 (x, y, z);
					}
				}
			}
		}

		for (int i = 0; i < bench.Length; i++) {
			DateTime start = DateTime.Now;
			PathFinder.FindPath (world, Point3D.Zero, new Point3D (5, 8, 9));
			TimeSpan ts = DateTime.Now - start;
			bench [i] = ts.TotalMilliseconds;
		}

		LineRenderer line = GetComponent<LineRenderer> ();

		print ("Total time: " + bench.Sum ().ToString () + "ms");
		print ("Average time: " + bench.Sum () / bench.Length + "ms");
		print ("Max: " + bench.Max () + "ms");
		print ("Min: " + bench.Min () + "ms");

		print ("Output: ");
		SearchNode crumb2 = PathFinder.FindPath (world, Point3D.Zero, new Point3D (5, 8, 9));
		if (crumb2 != null) {
			print ("Start: " + crumb2.position.ToString ());
			int index = 0;
			while (crumb2.next != null) {
				line.positionCount++;
				print ("Route: " + crumb2.next.position.ToString ());
				line.SetPosition (index, new Vector3 (crumb2.position.X, crumb2.position.Y, crumb2.position.Z));
				crumb2 = crumb2.next;
				index++;
			}
			print ("Finished at: " + crumb2.position.ToString ());            
			Console.ReadLine ();
		} else {
			print ("No path found");
		}
	}
}
