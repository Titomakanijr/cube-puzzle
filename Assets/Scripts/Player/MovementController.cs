﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour {
	//public variables
	int fromWaypointIndex;
	float percentBetweenWaypoints;

	public Vector3[] globalWaypoints = new Vector3[2];

	//Return new velocity based on distance between two waypoints
	public Vector3 CalculateMovement(float moveSpeed){

		fromWaypointIndex %= globalWaypoints.Length;
		int toWaypointIndex = (fromWaypointIndex + 1) % globalWaypoints.Length;
		//distance between from waypoint and to waypoint
		float distanceBetweenWaypoints = Vector3.Distance (globalWaypoints [fromWaypointIndex], globalWaypoints [toWaypointIndex]);
		//get the percent the object is between those two waypoints using the speed and deltatime
		percentBetweenWaypoints += Time.deltaTime * moveSpeed / distanceBetweenWaypoints;
		percentBetweenWaypoints = Mathf.Clamp01 (percentBetweenWaypoints);

		//get a new position for the object for the next frame
		Vector3 newPos = Vector3.Lerp (globalWaypoints [fromWaypointIndex], globalWaypoints [toWaypointIndex], percentBetweenWaypoints);
		if (percentBetweenWaypoints >= 1) {
			percentBetweenWaypoints = 0;
		}
		//return the velocity to get to the new position
		return newPos - transform.position;
	}
}
