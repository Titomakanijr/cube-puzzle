﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour {

	public struct MeshUtil {

		public enum Direction {
			Top = 0,
			Bottom = 1,
			Left = 2,
			Right = 3,
			Front = 4,
			Back = 5,
		}


		public static Mesh GetChildrenMesh (Transform tf) {
			if (tf) {
				MeshFilter meshF = tf.GetComponentInChildren<MeshFilter>();
				if (meshF) {
					return meshF.sharedMesh;
				}
			}
			return null;
		}


		public static void GetMeshMargin (Mesh mesh, out Vector3 min, out Vector3 max) {
			if (!mesh) {
				min = max = Vector3.zero;
				return;
			}
			min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
			Vector3[] vs = mesh.vertices;
			foreach (Vector3 v in vs) {
				min.x = Mathf.Min(min.x, v.x);
				min.y = Mathf.Min(min.y, v.y);
				min.z = Mathf.Min(min.z, v.z);
				max.x = Mathf.Max(max.x, v.x);
				max.y = Mathf.Max(max.y, v.y);
				max.z = Mathf.Max(max.z, v.z);
			}
		}
		public static void GetMeshMargin (Transform tf, out Vector3 min, out Vector3 max) {
			GetMeshMargin(GetChildrenMesh(tf), out min, out max);
		}

		public static Vector3 GetMeshSize (Mesh mesh) {
			Vector3 a, b;
			GetMeshMargin(mesh, out a, out b);
			return b - a;
		}
		public static Vector3 GetMeshSize (Transform tf) {
			return GetMeshSize(GetChildrenMesh(tf));
		}

		public static void GetExtremeMargin (Mesh mesh, out Vector3 min, out Vector3 max, Direction dir) {
			if (!mesh) {
				min = max = Vector3.zero;
				return;
			}
			min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
			Vector3 meshMin, meshMax;
			GetMeshMargin(mesh, out meshMin, out meshMax);
			Vector3[] vs = mesh.vertices;
			foreach (Vector3 v in vs) {
				if (dir == Direction.Top && v.y == meshMax.y ||
					dir == Direction.Bottom && v.y == meshMin.y ||
					dir == Direction.Left && v.x == meshMin.x ||
					dir == Direction.Right && v.x == meshMax.x ||
					dir == Direction.Front && v.z == meshMax.z ||
					dir == Direction.Back && v.z == meshMin.z
				) {
					min.x = Mathf.Min(min.x, v.x);
					min.y = Mathf.Min(min.y, v.y);
					min.z = Mathf.Min(min.z, v.z);
					max.x = Mathf.Max(max.x, v.x);
					max.y = Mathf.Max(max.y, v.y);
					max.z = Mathf.Max(max.z, v.z);
				}
			}
		}
		public static void GetExtremeMargin (Transform tf, out Vector3 min, out Vector3 max, Direction dir) {
			GetExtremeMargin(GetChildrenMesh(tf), out min, out max, dir);
		}

		public static Vector3 GetExtremeAverage (Mesh mesh, Direction dir) {
			Vector3 a, b;
			GetExtremeMargin(mesh, out a, out b, dir);
			return (a + b) * 0.5f;
		}
		public static Vector3 GetExtremeAverage (Transform tf, Direction dir) {
			return GetExtremeAverage(GetChildrenMesh(tf), dir);
		}

		public static Vector3 GetExtremeSize (Mesh mesh, Direction dir) {
			Vector3 a, b;
			GetExtremeMargin(mesh, out a, out b, dir);
			return b - a;
		}
		public static Vector3 GetExtremeSize (Transform tf, Direction dir) {
			return GetExtremeSize(GetChildrenMesh(tf), dir);
		}


	}
}
