﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections.Generic;

public class Player : MonoBehaviour {
	//public variables
	public float moveSpeed = 3.0f;

	//private Variables
	float fallTime = .85f;
	float fallTimer;
	bool moving;
	bool horizon;
	bool vert;
	bool menu;
	bool startFalling;
	bool checkEdgeFalling;
	bool falling;
	bool dropped;
	bool hangingEdgeBroke;
	bool movingToEdge;
	Point3D endCube;
	Point3D playerTmp;
	Point3D endFallPos;
	Cube crushCube;
	MovementController moveCont;
	SearchNode pathNode;
	WorldController worldCont;
	Animator anim;
	LevelCameraController levelCam;
	LevelController levelCont;
	Voxelizer vox;
	Rigidbody rigBody;

	/*	<Summary>
	 * 	Called when gameObject starts, sets timers and finds components
	 * 	</Summary>
	 */
	void Start(){
		moveCont = gameObject.GetComponent<MovementController> ();
		moveCont.globalWaypoints [1] = transform.position;
		worldCont = FindObjectOfType<WorldController> ();
		vox = FindObjectOfType<Voxelizer> ();
		anim = GetComponent<Animator> ();
		rigBody = GameObject.Find ("GameManager").GetComponent<Rigidbody> ();
		menu = true;
		movingToEdge = false;
		fallTimer = -1;
		levelCont = FindObjectOfType<LevelController> ();
	}

	void Update(){
		if (transform.position.y < 0) {
			Die ("falling");
		}
		//If we are at the next waypoint
		if (transform.position == moveCont.globalWaypoints [1]) {
			if (!falling && !movingToEdge && transform.position.y % 1 != .5f && worldCont.PositionIsFree (new Point3D (transform.position.x, transform.position.y - 1, transform.position.z))) {
				print (transform.position.y % 1);
				//If we are standing above where a cube should be (not on an edge)
				if (transform.position.x % 1 == 0 && transform.position.z % 1 == 0 && transform.position.y % 1 == 0) {
					//there was no cube, start falling
					if (fallTimer < 0 && !dropped) {
						fallTimer = fallTime;
					}
					falling = true;
					moving = false;
				}
			} else if(transform.position.y % 1 == .5f){
				//Player is on an edge
				if (checkEdgeFalling) {
					if (transform.position.z % 1 == .5f) { //holding onto the front or back of a cube
						if (worldCont.PositionIsFree (new Point3D (transform.position.x, transform.position.y - .5f, transform.position.z + .5f)) && worldCont.PositionIsFree (new Point3D (transform.position.x, transform.position.y - .5f, transform.position.z - .5f))) {
							falling = true;
							hangingEdgeBroke = true;
							fallTimer = -1;
						}
					} else { //holding onto the side of a cube
						if (worldCont.PositionIsFree (new Point3D (transform.position.x + .5f, transform.position.y - .5f, transform.position.z)) && worldCont.PositionIsFree (new Point3D (transform.position.x - .5f, transform.position.y - .5f, transform.position.z))) {
							falling = true;
							hangingEdgeBroke = true;
							fallTimer = -1;
						}
					}
					checkEdgeFalling = false;
				}
			}else {
				//there is a cube, check if we were falling
				if (falling && !worldCont.PositionIsFree (new Point3D (transform.position.x, transform.position.y - 1, transform.position.z))) {
					//standing on a cube, not falling anymore
					falling = false;
				}
			}
		}
		if (falling) {
			if (fallTimer > 0) {
				fallTimer -= Time.deltaTime;
			} else {
				if (transform.position == moveCont.globalWaypoints [1]) {
					playerTmp = new Point3D (transform.position.x, transform.position.y, transform.position.z);
					//Check every falling position to see if the player can move to a non-falling position
					for (int i = 0; i < fallingEdges.Length; i++) {
						endFallPos = new Point3D (playerTmp, fallingEdges [i].Point);
						UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
						if (pathNode == null) {
							continue;
						} else {
							falling = false;
							break;
						}
					}
					if (pathNode == null) {
						falling = true;
						if (hangingEdgeBroke) {

						} else {
							endFallPos = new Point3D (playerTmp, new Point3D (0, -1, 0));
						}
						UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
					} 
				}
			}
		} 
		//There is a node for movement, but we haven't started the movement yet
		if (pathNode != null && !moving) {
			//If there is a next node and the starting node is the player position we want to skip it
			Vector3 pathNodePos = new Vector3 (pathNode.position.X, pathNode.position.Y, pathNode.position.Z);
			if (pathNodePos == transform.position && pathNode.next != null) {
				moveCont.globalWaypoints [1] = new Vector3 (pathNode.next.position.X, pathNode.next.position.Y, pathNode.next.position.Z); 
			} else {
				moveCont.globalWaypoints [1] = pathNodePos; 
			}
			//Set the first waypoints for the movement
			moveCont.globalWaypoints [0] = transform.position;
			if (!movingToEdge) {
				pathNode = pathNode.next;
			}
			moving = true;
			SetDirection (moveCont.globalWaypoints [0], moveCont.globalWaypoints [1]);
		} 
		//The player has started a movement and will continue it until it is finished.
		if (moving) {
			if (!falling) {
				anim.SetBool ("Running", true);
			}
			
			Vector3 velocity = moveCont.CalculateMovement (moveSpeed);
			transform.Translate (velocity, Space.World);
			if (!menu) {
				levelCam.UpdateCameraTransform (velocity);
			}
			//Player object reached the waypoint
			if (transform.position == moveCont.globalWaypoints [1]) {
				//Check if we are at the main menu
				if (!menu) {
					//Check which cube the player is standing on
					if (!(worldCont.PositionIsFree (new Point3D (transform.position.x, transform.position.y - 1, transform.position.z)))) {
						Collider col = Physics.OverlapSphere (new Vector3 (transform.position.x, transform.position.y - 1, transform.position.z), .25f).First ();
						Cube standCube = col.gameObject.GetComponent<Cube> ();
						//pathfinding always starts at player position, which we don't want to bob
						standCube.Bob ();
						if (standCube.type == "Finish") {
							levelCont.FinishLevel ();
						} else if (standCube.type == "Bomb" || standCube.type == "Trap") {
							standCube.SetActive ();
						} else if (standCube.type == "Cracked") {
							standCube.SteppedOn ();
						}
					} 
				}
				if (pathNode != null && pathNode.next != null) {
					pathNode = pathNode.next;
					if (pathNode.next == null && !worldCont.PositionIsFree (pathNode.position)) {
						moving = false;
						pathNode = null;
					} else {
						moveCont.globalWaypoints [0] = transform.position;
						moveCont.globalWaypoints [1] = new Vector3 (pathNode.position.X, pathNode.position.Y, pathNode.position.Z);
						SetDirection (moveCont.globalWaypoints [0], moveCont.globalWaypoints [1]);
					}
				} else{
					//Movement is finished
					anim.SetBool("Running", false);
					pathNode = null;
					moving = false;
					movingToEdge = false;
					if (SceneManager.GetActiveScene ().name == "Main Menu") {
						transform.localEulerAngles = Vector3.zero;
					}
				}
			}
		}
	}

	/// <summary>
	/// Sets the rotational direction of the player
	/// </summary>
	/// <param name="start">Start position</param>
	/// <param name="end">End Position</param>
	public void SetDirection(Vector3 start, Vector3 end){
		if (start.x != end.x) {
			if (start.x > end.x) {
				if (end.y % 1 == .5f) {
					if (start.y % 1 == .5f) {

					} else {
						if (start.y > end.y) {
							transform.localEulerAngles = new Vector3 (0, -90, 0);
						} else {
							transform.localEulerAngles = new Vector3 (0, 90, 0);
						}
					}
				} else {
					transform.localEulerAngles = new Vector3 (0, 90, 0);
				}
			} else {
				if (end.y % 1 == .5f) {
					if (start.y > end.y) {
						transform.localEulerAngles = new Vector3 (0, 90, 0);
					} else {
						transform.localEulerAngles = new Vector3 (0, -90, 0);
					}
				} else {
					transform.localEulerAngles = new Vector3 (0, -90, 0);
				}
			}
		} else if (start.z != end.z){
			if (start.z > end.z) {
				if (end.y % 1 == .5f) {
					if (start.y > end.y) {
						transform.localEulerAngles = new Vector3 (0, -180, 0);
					} else {
						transform.localEulerAngles = new Vector3 (0, 0, 0);
					}
				} else {
					transform.localEulerAngles = new Vector3 (0, 0, 0);
				}
			} else {
				if (end.y % 1 == .5f) {
					if (start.y > end.y) {
						transform.localEulerAngles = new Vector3 (0, 0, 0);
					} else {
						transform.localEulerAngles = new Vector3 (0, -180, 0);
					}
				} else {
					transform.localEulerAngles = new Vector3 (0, -180, 0);
				}
			}
		}
	}

	/*	
	 *	<Summary>
	 *	Take in a SearchNode which is then set to the Player's initial pathNode for movement
	 *	</Summary>
	 */
	public void UpdatePathFindingNode(SearchNode startNode, Point3D cubePos){
		pathNode = startNode;
		endCube = cubePos;
	}

	/*	
	 *	<Summary>
	 *	Take in a movePos searchNode for a push movement, check if we need to adjust the position to the edge
	 *	</Summary>
	 */
	public void UpdatePushNode(SearchNode startNode, int dir, bool horiz){
		SearchNode edgeNode;
		if(worldCont.PositionIsFree(new Point3D(startNode.position.X, startNode.position.Y - 1, startNode.position.Z))){
			movingToEdge = true;
			if(horiz){
				edgeNode = new SearchNode (new Point3D (startNode.position.X + (.5f * dir), startNode.position.Y - .5f, startNode.position.Z), 0, 0, null);
				startNode.next = edgeNode;
			} else {
				edgeNode = new SearchNode (new Point3D (startNode.position.X , startNode.position.Y - .5f, startNode.position.Z + (.5f * dir)), 0, 0, null);
				startNode.next = edgeNode;
			}
		}
		pathNode = startNode;
	}

	/*
	 * 	<Summary>
	 * 	Getter function that returns a Vector3 of the next waypoint the player is moving to
	 * 	Used to calculate new paths during player movement
	 * 	</Summary>
	 */
	public Vector3 GetNextWaypoint(){
		return moveCont.globalWaypoints [1];
	}

	public void SetStart(Vector3 startPos){
		transform.position = startPos;
		pathNode = null;
		if (moveCont == null) {
			moveCont = gameObject.GetComponent<MovementController> ();
		}
		moveCont.globalWaypoints [0] = startPos;
		moveCont.globalWaypoints [1] = startPos;
	}

	public void AttemptFallToEdge(){
		dropped = true;
		playerTmp = new Point3D (transform.position.x, transform.position.y, transform.position.z);
		for (int i = 0; i < fallingEdges.Length; i++) {
			endFallPos = new Point3D (playerTmp, fallingEdges [i].Point);
			UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
			if (pathNode == null) {
				continue;
			} else {
				falling = false;
				break;
			}
		}
	}

	public void AttemptDropToEdge(string edge){
		playerTmp = new Point3D (transform.position.x, transform.position.y, transform.position.z);
		if (edge == "front") {
			endFallPos = new Point3D(playerTmp, fallingEdges [0].Point);
		} else if (edge == "left") {
			endFallPos = new Point3D(playerTmp,fallingEdges [1].Point);
		} else if (edge == "right") {
			endFallPos = new Point3D(playerTmp,fallingEdges [2].Point);
		} else if (edge == "back") {
			endFallPos = new Point3D(playerTmp,fallingEdges [3].Point);
		}
		UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
		if (pathNode != null) {
			falling = false;
		}
	}

	public void FallFromBrokenCube(){
		falling = true;
	}

	public void AttemptLedgeMovement(bool tryLeft){
		playerTmp = new Point3D (transform.position.x, transform.position.y, transform.position.z);
		if (tryLeft) {
			for (int i = 0; i < leftEdges.Length; i++) {
				if (leftEdges [i].Point.X == -1 && playerTmp.Z % 1 == 0) {
					continue;
				}
				endFallPos = new Point3D (playerTmp, leftEdges [i].Point);
				UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
				if (pathNode == null) {
					continue;
				} else {
					break;
				}
			}
		} else {
			for (int i = 0; i < rightEdges.Length; i++) {
				if (rightEdges [i].Point.X == 1 && playerTmp.Z % 1 == 0) {
					continue;
				}
				endFallPos = new Point3D (playerTmp, rightEdges [i].Point);
				UpdatePathFindingNode (PathFinder.FindPath (worldCont.world, playerTmp, endFallPos, falling), endFallPos);
				if (pathNode == null) {
					continue;
				} else {
					break;
				}
			}
		}
	}
		
	public void Die(string deathType, float bombX = 0, float bombY = 0, float bombZ = 0){
		pathNode = null;
		moving = false;
		vox.PlayerDeath (gameObject);
		print (deathType);
		//DESTROY PLAYER GAMEOBJECT
		/*if (deathType == "falling") {
		} else if (deathType == "bomb") {
			
		} else if (deathType == "spike" || deathType == "crush") {
		}*/
	}

	/*
	 * 	<Summary>
	 * 	Returns true if player object has a path node it is currently travelling on
	 * 	</Summary>
	 */
	public bool HasPathNode(){
		return pathNode != null;
	}

	public Point3D GetEndCube(){
		return endCube;
	}

	public void SetMenu(bool value){
		menu = value;
	}

	public void SetWorldController(WorldController worldC){
		worldCont = worldC;
	}

	public World GetWorld(){
		return worldCont.world;
	}

	public void SetCamera(LevelCameraController newCam){
		levelCam = newCam;
	}

	public bool GetFalling(){
		return falling;
	}

	public void CheckEdgeFalling(){
		checkEdgeFalling = true;
	}


	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.name.Contains("Cube")){
			crushCube = collider.gameObject.GetComponent<Cube> ();;
			//If the cube is falling and above the players position
			if (crushCube.falling && transform.position.y < crushCube.transform.position.y) {
				//Kill the player
				Die ("crush");
			}
		}
	}

	//edges that will be used when falling or attempt to fall to edge
	private static Surr[] fallingEdges = new Surr[]{      
		//y != 0, moves up and down
		new Surr(0,-.5f,-.5f), new Surr(-.5f,-.5f,0), 
		new Surr(.5f,-.5f,0), new Surr(0,-.5f,.5f)
	};

	private static Surr[] rightEdges = new Surr[]{      
		//y != 0, moves up and down
		new Surr(1,0,0), new Surr(.5f,0,-.5f),
		new Surr(.5f,0,.5f)
	};

	private static Surr[] leftEdges = new Surr[]{      
		//y != 0, moves up and down
		new Surr(-1,0,0), new Surr(-.5f,0,-.5f),
		new Surr(-.5f,0,.5f)
	};
}