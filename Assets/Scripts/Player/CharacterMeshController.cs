﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(Animator))]
public class CharacterMeshController : MonoBehaviour {

	[System.Serializable]
	public struct CharacterModel {
		public GameObject Hat, Head, ArmL, ArmR, HandL, HandR;
		public GameObject Body, LegL, LegR, FootL, FootR;
	}

	public struct CharacterModelTrans {
		public Transform Hat, Head, ArmL, ArmR, HandL, HandR;
		public Transform Body, LegL, LegR, FootL, FootR;
	}
	public CharacterModel chModel;
	public CharacterModelTrans chMTrans;

	// Use this for initialization
	void Start () {
		GameObject player = GameObject.Find ("Player");

		//Create game game objects from model prefabs
		if (chModel.Body) { 
			chModel.Body = Instantiate (chModel.Body);
			chModel.Body.name = chModel.Body.name.Replace ("(Clone)", "_Model").Trim ();
			chModel.Body.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.Head) { 
			chModel.Head = Instantiate (chModel.Head);
			chModel.Head.name = chModel.Head.name.Replace ("(Clone)", "_Model").Trim ();
			chModel.Head.transform.localRotation = Quaternion.identity; 
		}
		if (chModel.ArmL) { 
			chModel.ArmL = Instantiate (chModel.ArmL);
			chModel.ArmL.name = chModel.ArmL.name.Replace ("(Clone)", "L_Model").Trim ();
			chModel.ArmL.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.ArmR) { 
			chModel.ArmR = Instantiate (chModel.ArmR);
			chModel.ArmR.name = chModel.ArmR.name.Replace ("(Clone)", "R_Model").Trim ();
			chModel.ArmR.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.HandL) { 
			chModel.HandL = Instantiate (chModel.HandL);
			chModel.HandL.name = chModel.HandL.name.Replace ("(Clone)", "L_Model").Trim ();
			chModel.HandL.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.HandR) { 
			chModel.HandR = Instantiate (chModel.HandR);
			chModel.HandR.name = chModel.HandR.name.Replace ("(Clone)", "R_Model").Trim ();
			chModel.HandR.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.LegL) { 
			chModel.LegL = Instantiate (chModel.LegL);
			chModel.LegL.name = chModel.LegL.name.Replace ("(Clone)", "L_Model").Trim ();
			chModel.LegL.transform.localRotation = Quaternion.identity; 
			chModel.Body.transform.localPosition = Vector3.zero; 
		}
		if (chModel.LegR) { 
			chModel.LegR = Instantiate (chModel.LegR);
			chModel.LegR.name = chModel.LegR.name.Replace ("(Clone)", "R_Model").Trim ();
			chModel.LegR.transform.localRotation = Quaternion.identity; 
		}
		if (chModel.FootL) { 
			chModel.FootL = Instantiate (chModel.FootL);
			chModel.FootL.name = chModel.FootL.name.Replace ("(Clone)", "L_Model").Trim ();
			chModel.FootL.transform.localRotation = Quaternion.identity; 
		}
		if (chModel.FootR) { 
			chModel.FootR = Instantiate (chModel.FootR);
			chModel.FootR.name = chModel.FootR.name.Replace ("(Clone)", "R_Model").Trim ();
			chModel.FootR.transform.localRotation = Quaternion.identity; 
		}
		if (chModel.Hat) { 
			chModel.Hat = Instantiate (chModel.Hat);
			chModel.Hat.name = chModel.Hat.name.Replace ("(Clone)", "_Model").Trim ();
			chModel.Hat.transform.localRotation = Quaternion.identity; 
		}

		chMTrans.Body = new GameObject ("Body").transform;
		chMTrans.Hat = new GameObject ("Hat").transform;
		chMTrans.Head = new GameObject ("Head").transform;
		chMTrans.ArmL = new GameObject ("ArmL").transform;
		chMTrans.ArmR = new GameObject ("ArmR").transform;
		chMTrans.LegL = new GameObject ("LegL").transform;
		chMTrans.LegR = new GameObject ("LegR").transform;
		chMTrans.HandL = new GameObject ("HandL").transform;
		chMTrans.HandR = new GameObject ("HandR").transform;
		chMTrans.FootL = new GameObject ("FootL").transform;
		chMTrans.FootR = new GameObject ("FootR").transform;

		chMTrans.Body.SetParent(player.transform);
		chMTrans.Head.SetParent(chMTrans.Body);
		chMTrans.Hat.SetParent(chMTrans.Head);
		chMTrans.ArmL.SetParent(chMTrans.Body);
		chMTrans.ArmR.SetParent(chMTrans.Body);
		chMTrans.LegL.SetParent(chMTrans.Body);
		chMTrans.LegR.SetParent(chMTrans.Body);
		chMTrans.HandL.SetParent(chMTrans.ArmL);
		chMTrans.HandR.SetParent(chMTrans.ArmR);
		chMTrans.FootL.SetParent(chMTrans.LegL);
		chMTrans.FootR.SetParent(chMTrans.LegR);

		chModel.Body.transform.SetParent(chMTrans.Body);
		if (chModel.Head) {
			chModel.Head.transform.SetParent (chMTrans.Head);
		}
		if (chModel.Hat) {
			chModel.Hat.transform.SetParent (chMTrans.Hat);
		}
		chModel.ArmL.transform.SetParent(chMTrans.ArmL);
		chModel.ArmR.transform.SetParent(chMTrans.ArmR);
		chModel.LegL.transform.SetParent(chMTrans.LegL);
		chModel.LegR.transform.SetParent(chMTrans.LegR);
		chModel.HandL.transform.SetParent(chMTrans.HandL);
		chModel.HandR.transform.SetParent(chMTrans.HandR);
		chModel.FootL.transform.SetParent(chMTrans.FootL);
		chModel.FootR.transform.SetParent(chMTrans.FootR);

		Vector3 bodyPos, headPos, hatPos, ArmPos, LegPos, HandPos, FootPos;
		Vector3 bodyMPos, headMPos, hatMPos, ArmMPos, LegMPos, HandMPos, FootMPos;

		Vector3 bodySize = Utils.MeshUtil.GetMeshSize(chModel.Body.transform);
		Vector3 legSize = Utils.MeshUtil.GetMeshSize(chModel.LegL.transform);
		Vector3 footSize = Utils.MeshUtil.GetMeshSize(chModel.FootL.transform);
		Vector3 armSize = Utils.MeshUtil.GetMeshSize(chModel.ArmL.transform);
		Vector3 handSize = Utils.MeshUtil.GetMeshSize(chModel.HandL.transform);
		Vector3 headSize, headBAvr;
		headSize = Vector3.zero;
		headBAvr = Vector3.zero;
		headPos = Vector3.zero;
		headMPos = Vector3.zero;
		if (chModel.Head) {
			headSize = Utils.MeshUtil.GetMeshSize (chModel.Head.transform);
			headBAvr = Utils.MeshUtil.GetExtremeAverage (chModel.Head.transform, Utils.MeshUtil.Direction.Bottom);
		}
		Vector3 bodyLMin, bodyLMax;
		Utils.MeshUtil.GetExtremeMargin(chModel.Body.transform, out bodyLMin, out bodyLMax, Utils.MeshUtil.Direction.Left);
		Vector3 bodyBMin, bodyBMax;
		Utils.MeshUtil.GetExtremeMargin(chModel.Body.transform, out bodyBMin, out bodyBMax, Utils.MeshUtil.Direction.Bottom);
		Vector3 footTAvr = Utils.MeshUtil.GetExtremeAverage(chModel.FootL.transform, Utils.MeshUtil.Direction.Top);
		Vector3 handBAvr = Utils.MeshUtil.GetExtremeAverage(chModel.HandL.transform, Utils.MeshUtil.Direction.Bottom);
		Vector3 bodyTAvr = Utils.MeshUtil.GetExtremeAverage(chModel.Body.transform, Utils.MeshUtil.Direction.Top);

		bodyPos = new Vector3(0, -.5f, 0);
		bodyMPos = new Vector3(0f, legSize.y + footSize.y, 0f);
		if (chModel.Head) {
			headPos = new Vector3 (0f, bodyMPos.y + bodySize.y, bodyTAvr.z);
			headMPos = new Vector3 (0f, 0f, -headBAvr.z);
			hatMPos = new Vector3 (0f, headSize.y, 0f);
		} else {
			hatMPos = new Vector3 (0, bodyMPos.y+ bodySize.y + .8f, 0f);
		}
		hatPos = Vector3.zero;
		ArmPos = new Vector3(-bodySize.x * 0.5f - armSize.x * 0.5f, bodyMPos.y + bodyLMax.y - 0.05f, 0f);
		ArmMPos = new Vector3(0f, -armSize.y, 0f);
		HandPos = new Vector3(0f, -armSize.y, 0f);
		HandMPos = new Vector3(0f, -handSize.y, -handBAvr.z);
		LegPos = new Vector3(bodyBMin.x + 0.05f, legSize.y + footSize.y, 0f);
		LegMPos = new Vector3(0f, -legSize.y, 0f);
		FootPos = new Vector3(0f, -legSize.y, 0f);
		FootMPos = new Vector3(0f, -footSize.y, -footTAvr.z);

		//Set containing game object positions
		chMTrans.Body.transform.localPosition = bodyPos;
		chMTrans.Head.transform.localPosition = headPos;
		chMTrans.Hat.transform.localPosition = hatPos;
		chMTrans.ArmL.transform.localPosition = ArmPos;
		ArmPos.x *= -1f;
		chMTrans.ArmR.transform.localPosition = ArmPos;
		chMTrans.LegL.transform.localPosition = LegPos;
		LegPos.x *= -1f;
		chMTrans.LegR.transform.localPosition = LegPos;
		chMTrans.HandL.transform.localPosition = HandPos;
		chMTrans.HandR.transform.localPosition = HandPos;
		chMTrans.FootL.transform.localPosition = FootPos;
		chMTrans.FootR.transform.localPosition = FootPos;

		//Set Model positions
		chModel.Body.transform.localPosition = bodyMPos;
		if (chModel.Head) {
			chModel.Head.transform.localPosition = headMPos;
		}
		if (chModel.Hat) {
			chModel.Hat.transform.localPosition = hatMPos;
		}
		chModel.ArmL.transform.localPosition = ArmMPos;
		chModel.ArmR.transform.localPosition = ArmMPos;
		chModel.LegL.transform.localPosition = LegMPos;
		chModel.LegR.transform.localPosition = LegMPos;
		chModel.HandL.transform.localPosition = HandMPos;
		chModel.HandR.transform.localPosition = HandMPos;
		chModel.FootL.transform.localPosition = FootMPos;
		chModel.FootR.transform.localPosition = FootMPos;
		chMTrans.Body.transform.localScale = new Vector3 (.5f, .5f, .5f);
		//Restart player object so that the animations play properly
		player.SetActive (false);
		player.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
