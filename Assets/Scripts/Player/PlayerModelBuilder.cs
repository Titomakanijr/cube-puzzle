﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelBuilder : MonoBehaviour {

	struct CharacterModelTrans {
		public Transform Hat, Head, ArmL, ArmR, HandL, HandR;
		public Transform Body, LegL, LegR, FootL, FootR;
	}

	bool init;
	bool retry;
	bool restartNext, restart;
	CharacterModelTrans chMTrans;
	Shop[] shops = new Shop[3];
	GameObject player;
	DataManager dataMan;

	// Use this for initialization
	void Start () {
		init = true;
		player = GameObject.Find ("Player");
		shops [0] = GameObject.Find ("BodyShop").GetComponent<Shop>();
		shops [1] = GameObject.Find ("HeadShop").GetComponent<Shop>();
		shops [2] = GameObject.Find ("HatShop").GetComponent<Shop>();
		dataMan = FindObjectOfType<DataManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		//If we initilize during start shop menu lists will be null, instead we will initilize on the first frame
		if (init) {
			BuildModel ();
			init = false;
		}
		if (restart) {
			player.SetActive (false);
			player.SetActive (true);
			restart = false;
		}else if (restartNext) {
			restart = true;
			restartNext = false;
		}
	}

	public void BuildModel(){
		retry = true;
		string bodyName = dataMan.GetBody ();
		string headName = dataMan.GetHead ();
		string hatName = dataMan.GetHat ();

		if(player.transform.Find("Body")){
			Destroy(GameObject.Find("Player/Body"));
		}
		while (retry) {
			try {
				for (int i = 0; i < shops.Length; i++) {
					List<SwipeMenuItem> menuItems = shops [i].GetOwnedItems ();
					for (int j = 0; j < menuItems.Count; j++) {
						if (i == 0) {
							if (menuItems [j].GetModelName () == bodyName) {
								GameObject body = Instantiate (menuItems [j].smCont.chMTrans.Body.gameObject);
								chMTrans.Body = body.transform;
								chMTrans.LegL = body.transform.Find ("LegL");
								chMTrans.LegR = body.transform.Find ("LegR");
								chMTrans.FootL = body.transform.Find ("LegL/FootL");
								chMTrans.FootR = body.transform.Find ("LegR/FootR");
								chMTrans.ArmL = body.transform.Find ("ArmL");
								chMTrans.ArmR = body.transform.Find ("ArmR");
								chMTrans.HandL = body.transform.Find ("ArmL/HandL");
								chMTrans.HandR = body.transform.Find ("ArmR/HandR");

								chMTrans.Body.SetParent (player.transform);
								break;
							}
						} else if (i == 1) {
							if (menuItems [j].GetModelName () == headName) {
								GameObject Head = Instantiate (menuItems [j].smCont.chMTrans.Head.gameObject);
								chMTrans.Head = Head.transform;

								chMTrans.Head.SetParent (chMTrans.Body);
								break;
							}
						} else {
							if (menuItems [j].GetModelName () == hatName) {
								chMTrans.Hat = menuItems [j].smCont.chMTrans.Hat;
								if (chMTrans.Hat != null) {
									GameObject Hat = Instantiate (menuItems [j].smCont.chMTrans.Hat.gameObject);
									chMTrans.Hat = Hat.transform;
									chMTrans.Hat.SetParent (chMTrans.Head);
								}
								break;
							}
						}
					}
				}
			

				Vector3 bodyPos, headPos, hatPos;

				Vector3 headSize, headTAvr;
				headSize = Vector3.zero;
				headTAvr = Vector3.zero;
				headPos = Vector3.zero;

				Transform temp = chMTrans.Body.Find ("Body_Model");
				Vector3 bodySize = Utils.MeshUtil.GetMeshSize (temp);
				Vector3 bodyTAvr = Utils.MeshUtil.GetExtremeAverage (temp.transform, Utils.MeshUtil.Direction.Top);
				bodyPos = new Vector3 (0, -.5f, 0);
				headPos = new Vector3 (0f, bodySize.y + temp.localPosition.y, bodyTAvr.z);
				temp = chMTrans.Head.Find ("Head_Model");
				headSize = Utils.MeshUtil.GetMeshSize (temp);
				headTAvr = Utils.MeshUtil.GetExtremeAverage (temp.transform, Utils.MeshUtil.Direction.Top);
				hatPos = new Vector3 (0f, headSize.y, headTAvr.z);

				//Set containing game object positions
				chMTrans.Body.transform.localPosition = bodyPos;
				chMTrans.Head.transform.localPosition = headPos;
				if (chMTrans.Hat) {
					chMTrans.Hat.transform.localPosition = hatPos;
					chMTrans.Hat.name = chMTrans.Hat.name.Replace ("(Clone)", "").Trim ();
				}

				chMTrans.Body.name = chMTrans.Body.name.Replace ("(Clone)", "").Trim ();
				chMTrans.Head.name = chMTrans.Head.name.Replace ("(Clone)", "").Trim ();
				chMTrans.Body.localPosition = Vector3.zero;
				chMTrans.Body.localScale = new Vector3 (.5f, .5f, .5f);
				restartNext = true;
				retry = false;
			} catch {
				bodyName = "StandardMan";
				headName = "StandardMan";
				hatName = "";
				dataMan.SetHat (hatName);
				dataMan.SetHead (headName);
				dataMan.SetBody (bodyName);
			}
		}

	}
}
